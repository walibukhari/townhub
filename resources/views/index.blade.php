<!DOCTYPE html>
<html lang="en-US" class="no-js no-svg" itemscope>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="profile" href="//gmpg.org/xfn/11">
    <!-- <link media="all" href="https://townhub.cththemes.com/wp-content/cache/autoptimize/css/autoptimize_a8d0ed1e0fce3d1324f667583150910e.css" rel="stylesheet" /> -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/aae1p.css')}}" media="all" />
    <!-- <link media="only screen and (max-width: 768px)" href="https://townhub.cththemes.com/wp-content/cache/autoptimize/css/autoptimize_541e2ced151704f4ff1844c6de47ec02.css" rel="stylesheet" /> -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/a8k9h.css')}}" media="only screen and (max-width: 768px)" />
    <title>TownHub &#8211; Directory &amp; Listing WordPress Theme</title>
    <meta name='robots' content='max-image-preview:large' />
    <link rel='dns-prefetch' href='//api.mapbox.com' />
    <link rel='dns-prefetch' href='//cdn.jsdelivr.net' />
    <link rel='dns-prefetch' href='//www.google.com' />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link href='https://fonts.gstatic.com' crossorigin rel='preconnect' />
    <link rel="alternate" type="application/rss+xml" title="TownHub &raquo; Feed" href="https://townhub.cththemes.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title="TownHub &raquo; Comments Feed" href="https://townhub.cththemes.com/comments/feed/" />
    <!-- <link rel='stylesheet' id='listing_types-css'  href='https://townhub.cththemes.com/wp-content/cache/autoptimize/css/autoptimize_single_21950f23510a09a30c5ee0a98eff159e.css?ver=6.0' type='text/css' media='all' /> -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/a8k9h.min.css')}}" media="all" />
    <link rel='stylesheet' id='mapbox-gl-css' href='https://api.mapbox.com/mapbox-gl-js/v1.11.0/mapbox-gl.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='mapbox-gl-geocoder-css' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='townhub-fonts-css' href='https://fonts.googleapis.com/css?family=Raleway%3A300%2C400%2C700%2C800%2C900%7CRoboto%3A400%2C500%2C700%2C900&#038;display=swap&#038;subset=cyrillic%2Cvietnamese' type='text/css' media='all' />
    <link rel="stylesheet" type="text/css" href="{{asset('css/aae1p.min.css')}}" media="all" />
    <script src='{{asset('js/a8k9h.min.js')}}' type="text/javascript"></script>
    <link rel="https://api.w.org/" href="https://townhub.cththemes.com/wp-json/" />
    <link rel="alternate" type="application/json" href="https://townhub.cththemes.com/wp-json/wp/v2/pages/545" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://townhub.cththemes.com/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://townhub.cththemes.com/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 6.0" />
    <meta name="generator" content="WooCommerce 6.5.1" />
    <link rel="canonical" href="https://townhub.cththemes.com/" />
    <link rel='shortlink' href='https://townhub.cththemes.com/' />
    <link rel="alternate" type="application/json+oembed" href="https://townhub.cththemes.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ftownhub.cththemes.com%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://townhub.cththemes.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ftownhub.cththemes.com%2F&#038;format=xml" />
    <meta name="generator" content="Redux 4.3.14" />
    <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
            .listing-avatar img{
                border:1px solid #000 !important;
                border-radius: 100% !important;
            }
        </style>
    </noscript>
    <link rel="icon" href="https://townhub.cththemes.com/wp-content/uploads/2018/06/CTH-themes.jpg" sizes="32x32" />
    <link rel="icon" href="https://townhub.cththemes.com/wp-content/uploads/2018/06/CTH-themes.jpg" sizes="192x192" />
    <link rel="apple-touch-icon" href="https://townhub.cththemes.com/wp-content/uploads/2018/06/CTH-themes.jpg" />
    <meta name="msapplication-TileImage" content="https://townhub.cththemes.com/wp-content/uploads/2018/06/CTH-themes.jpg" />
</head>
<body class="home page-template page-template-home-fullscreen page-template-home-fullscreen-php page page-id-545 wp-custom-logo theme-townhub townhub-has-addons woocommerce-no-js body-townhub folio-archive- townhub-front-page woo-two woo-tablet-two elementor-default elementor-kit-9421 elementor-page elementor-page-545">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
    <defs>
        <filter id="wp-duotone-dark-grayscale">
            <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
            <feComponentTransfer color-interpolation-filters="sRGB">
                <feFuncR type="table" tableValues="0 0.49803921568627" />
                <feFuncG type="table" tableValues="0 0.49803921568627" />
                <feFuncB type="table" tableValues="0 0.49803921568627" />
                <feFuncA type="table" tableValues="1 1" />
            </feComponentTransfer>
            <feComposite in2="SourceGraphic" operator="in" />
        </filter>
    </defs>
</svg>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
    <defs>
        <filter id="wp-duotone-grayscale">
            <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
            <feComponentTransfer color-interpolation-filters="sRGB">
                <feFuncR type="table" tableValues="0 1" />
                <feFuncG type="table" tableValues="0 1" />
                <feFuncB type="table" tableValues="0 1" />
                <feFuncA type="table" tableValues="1 1" />
            </feComponentTransfer>
            <feComposite in2="SourceGraphic" operator="in" />
        </filter>
    </defs>
</svg>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
    <defs>
        <filter id="wp-duotone-purple-yellow">
            <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
            <feComponentTransfer color-interpolation-filters="sRGB">
                <feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" />
                <feFuncG type="table" tableValues="0 1" />
                <feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" />
                <feFuncA type="table" tableValues="1 1" />
            </feComponentTransfer>
            <feComposite in2="SourceGraphic" operator="in" />
        </filter>
    </defs>
</svg>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
    <defs>
        <filter id="wp-duotone-blue-red">
            <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
            <feComponentTransfer color-interpolation-filters="sRGB">
                <feFuncR type="table" tableValues="0 1" />
                <feFuncG type="table" tableValues="0 0.27843137254902" />
                <feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" />
                <feFuncA type="table" tableValues="1 1" />
            </feComponentTransfer>
            <feComposite in2="SourceGraphic" operator="in" />
        </filter>
    </defs>
</svg>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
    <defs>
        <filter id="wp-duotone-midnight">
            <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
            <feComponentTransfer color-interpolation-filters="sRGB">
                <feFuncR type="table" tableValues="0 0" />
                <feFuncG type="table" tableValues="0 0.64705882352941" />
                <feFuncB type="table" tableValues="0 1" />
                <feFuncA type="table" tableValues="1 1" />
            </feComponentTransfer>
            <feComposite in2="SourceGraphic" operator="in" />
        </filter>
    </defs>
</svg>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
    <defs>
        <filter id="wp-duotone-magenta-yellow">
            <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
            <feComponentTransfer color-interpolation-filters="sRGB">
                <feFuncR type="table" tableValues="0.78039215686275 1" />
                <feFuncG type="table" tableValues="0 0.94901960784314" />
                <feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" />
                <feFuncA type="table" tableValues="1 1" />
            </feComponentTransfer>
            <feComposite in2="SourceGraphic" operator="in" />
        </filter>
    </defs>
</svg>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
    <defs>
        <filter id="wp-duotone-purple-green">
            <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
            <feComponentTransfer color-interpolation-filters="sRGB">
                <feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" />
                <feFuncG type="table" tableValues="0 1" />
                <feFuncB type="table" tableValues="0.44705882352941 0.4" />
                <feFuncA type="table" tableValues="1 1" />
            </feComponentTransfer>
            <feComposite in2="SourceGraphic" operator="in" />
        </filter>
    </defs>
</svg>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
    <defs>
        <filter id="wp-duotone-blue-orange">
            <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
            <feComponentTransfer color-interpolation-filters="sRGB">
                <feFuncR type="table" tableValues="0.098039215686275 1" />
                <feFuncG type="table" tableValues="0 0.66274509803922" />
                <feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" />
                <feFuncA type="table" tableValues="1 1" />
            </feComponentTransfer>
            <feComposite in2="SourceGraphic" operator="in" />
        </filter>
    </defs>
</svg>
<!--loader-->
<div class="loader-wrap">
    <div class="loader-inner">
        <div class="loader-inner-cirle"></div>
    </div>
</div>
<!--loader end-->
<div id="main-theme">
    <!-- header-->
    <header id="masthead" class="townhub-header main-header dark-header fs-header sticky">
        <div class="logo-holder">
            <a href="https://townhub.cththemes.com/" class="custom-logo-link" rel="home" aria-current="page">
                <img width="411" height="77" class="custom-logo lazy" alt="TownHub" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/logo.png" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/logo.png" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
            </a>
        </div>
        <!-- header-search_btn-->
        <!-- header-search_btn-->
        <div class="header-search_btn show-search-button" data-optext="Search" data-cltext="Close">
            <i class="fal fa-search"></i>
            <span>Search</span>
        </div>
        <!-- header-search_btn end-->
        <!-- header-search_container -->
        <div class="header-search_container header-search vis-search">
            <div class="container small-container">
                <div class="header-search-input-wrap fl-wrap">
                    <form role="search" method="get" action="https://townhub.cththemes.com/" class="list-search-header-form list-search-form-js">
                        <div class="azp_element filter_form_hero azp-element-azp-nlmfa3g5mt">
                            <div class="hero-inputs-wrap fl-wrap">
                                <div class="azp_element filter_title azp-element-azp-ilzphdvswli filter-gid-item filter-gid-wid-4">
                                    <div class="filter-item-inner">
                                        <label class="flabel-icon">
                                            <i class="fal fa-keyboard"></i>
                                        </label>
                                        <input type="text" name="search_term" placeholder="What are you looking for?" value="" />
                                    </div>
                                </div>
                                <div class="azp_element filter_nearby azp-element-azp-6kdd9gqw5w3 filter-gid-item filter-gid-wid-4">
                                    <div class="filter-item-inner show-distance-filter nearby-inputs-wrap">
                                        <div class="nearby-input-wrap nearby-mapbox" id="nearby-wrap62a3d839c6793" data-placeholder="Location">
                                            <label class="flabel-icon">
                                                <i class="fal fa-map-marker"></i>
                                            </label>
                                            <input id="auto-place-loc62a3d839c6799" name="location_search" type="text" placeholder="Location" class="qodef-archive-places-search location-input auto-place-loc" value="" />
                                            <a href="#" class="get-current-city">
                                                <i class="far fa-dot-circle"></i>
                                            </a>
                                            <span class="autoplace-clear-input">
                                                        <i class="far fa-times"></i>
                                                    </span>
                                        </div>
                                        <input type="hidden" name="distance" value="10000">
                                        <input type="hidden" class="auto-place-nearby" name="nearby" value="off">
                                        <input type="hidden" class="address_lat auto-place-lat" name="address_lat" value="">
                                        <input type="hidden" class="address_lng auto-place-lng" name="address_lng" value="">
                                    </div>
                                </div>
                                <div class="azp_element filter_cat azp-element-azp-cl5jnwtre0t filter-gid-item filter-gid-wid-4">
                                    <div class="filter-item-inner">
                                        <select data-placeholder="All Categories" class="chosen-select" name="lcats[]">
                                            <option value="">All Categories</option>
                                            <option value="195">Hotels</option>
                                            <option value="361">Tour</option>
                                            <option value="382">Events</option>
                                            <option value="383">Restaurants</option>
                                            <option value="397">Cars</option>
                                            <option value="412">Shops</option>
                                            <option value="413">Fitness</option>
                                        </select>
                                    </div>
                                </div>
                                <button class="main-search-button color2-bg" type="submit">Search <i class="far fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="header-search_close color-bg">
                    <i class="fal fa-long-arrow-up"></i>
                </div>
            </div>
        </div>
        <!-- header-search_container  end -->
        <!-- header opt -->
        <a href="#" class="add-list color-bg logreg-modal-open" data-message="You must be logged in to add listings.">Add Listing <span>
                        <i class="fal fa-layer-plus"></i>
                    </span>
        </a>
        <div class="cart-btn bookmark-header-btn show-header-modal" data-microtip-position="bottom" role="tooltip" aria-label="Your Wishlist">
            <i class="fal fa-heart"></i>
            <span class="cart-counter bmcounter-head green-bg">0</span>
        </div>
        <!-- wishlist-wrap-->
        <div class="header-modal novis_wishlist">
            <!-- header-modal-container-->
            <div class="header-modal-container scrollbar-inner fl-wrap" data-simplebar>
                <!--widget-posts-->
                <div class="widget-posts  fl-wrap">
                    <ul class="no-list-style wishlist-items-wrap">
                        <li class="wishlist-item no-bookmark-wrap">
                            <p>You have no bookmark.</p>
                        </li>
                    </ul>
                </div>
                <!-- widget-posts end-->
            </div>
            <!-- header-modal-container end-->
            <div class="header-modal-top fl-wrap">
                <h4>Your Wishlist : <span class="bmcounter-bot">
                                <strong>0</strong> listings </span>
                </h4>
                <div class="close-header-modal">
                    <i class="far fa-times"></i>
                </div>
            </div>
        </div>
        <!--wishlist-wrap end -->
        <a href="#" class="show-reg-form avatar-img logreg-modal-open">
            <i class="fal fa-user"></i>Sign In </a>
        <!-- header opt end-->
        <div id="custom_html-7" class="widget_text townhub-lang-curr-wrap widget_custom_html">
            <div class="textwidget custom-html-widget">
                <div class="lang-wrap">
                    <div class="show-lang">
                                <span>
                                    <i class="fal fa-globe-europe"></i>
                                    <strong>En</strong>
                                </span>
                        <i class="fa fa-caret-down arrlan"></i>
                    </div>
                    <ul class="lang-tooltip lang-action no-list-style">
                        <li>
                            <a href="#" class="current-lan" data-lantext="En">English</a>
                        </li>
                        <li>
                            <a href="#" data-lantext="Fr">Français</a>
                        </li>
                        <li>
                            <a href="#" data-lantext="Es">Español</a>
                        </li>
                        <li>
                            <a href="#" data-lantext="De">Deutsch</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- lang-wrap-->
        <!-- lang-wrap end-->
        <!-- nav-button-wrap-->
        <div class="nav-button-wrap color-bg">
            <div class="nav-button">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- nav-button-wrap end-->
        <!--  .nav-holder -->
        <div class="nav-holder main-menu">
            <nav id="site-navigation" class="main-navigation" aria-label="Top Menu">
                <ul id="top-menu" class="menu">
                    <li id="menu-item-1316" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-545 current_page_item menu-item-has-children menu-item-1316">
                        <a href="https://townhub.cththemes.com/" aria-current="page">Home</a>
                        <ul class="sub-menu">
                            <li id="menu-item-6826" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6826">
                                <a href="https://townhub.cththemes.com/home-slider/">Slider</a>
                            </li>
                            <li id="menu-item-6825" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6825">
                                <a href="https://townhub.cththemes.com/home-slideshow/">Slideshow</a>
                            </li>
                            <li id="menu-item-6824" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6824">
                                <a href="https://townhub.cththemes.com/home-video/">Video</a>
                            </li>
                            <li id="menu-item-6823" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6823">
                                <a href="https://townhub.cththemes.com/home-map/">Map</a>
                            </li>
                        </ul>
                    </li>
                    <li id="menu-item-6798" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-6798">
                        <a href="https://townhub.cththemes.com/listings/">Listings</a>
                        <ul class="sub-menu">
                            <li id="menu-item-6804" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6804">
                                <a href="https://townhub.cththemes.com/listings/column-map/">Column Map</a>
                            </li>
                            <li id="menu-item-6803" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6803">
                                <a href="https://townhub.cththemes.com/listings/column-map-filter/">Column Map 2</a>
                            </li>
                            <li id="menu-item-6802" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6802">
                                <a href="https://townhub.cththemes.com/listings/full-map/">Full Map</a>
                            </li>
                            <li id="menu-item-6801" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6801">
                                <a href="https://townhub.cththemes.com/listings/full-map-filter/">Full Map 2</a>
                            </li>
                            <li id="menu-item-6800" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6800">
                                <a href="https://townhub.cththemes.com/listings/without-map/">Without Map</a>
                            </li>
                            <li id="menu-item-6799" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6799">
                                <a href="https://townhub.cththemes.com/listings/without-map-filter/">Without Map 2</a>
                            </li>
                            <li id="menu-item-2138" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2138">
                                <a href="#">Single</a>
                                <ul class="sub-menu">
                                    <li id="menu-item-6846" class="menu-item menu-item-type-post_type menu-item-object-listing menu-item-6846">
                                        <a href="https://townhub.cththemes.com/listing/the-goggi-restaurant/">Image</a>
                                    </li>
                                    <li id="menu-item-6843" class="menu-item menu-item-type-post_type menu-item-object-listing menu-item-6843">
                                        <a href="https://townhub.cththemes.com/listing/moonlight-hotel/">Carousel</a>
                                    </li>
                                    <li id="menu-item-6844" class="menu-item menu-item-type-post_type menu-item-object-listing menu-item-6844">
                                        <a href="https://townhub.cththemes.com/listing/premium-fitness-gym/">Slideshow</a>
                                    </li>
                                    <li id="menu-item-6845" class="menu-item menu-item-type-post_type menu-item-object-listing menu-item-6845">
                                        <a href="https://townhub.cththemes.com/listing/rocko-band-in-marquee-club/">Video</a>
                                    </li>
                                    <li id="menu-item-8999" class="menu-item menu-item-type-post_type menu-item-object-listing menu-item-8999">
                                        <a href="https://townhub.cththemes.com/listing/city-tour/">Map</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li id="menu-item-6897" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6897">
                        <a href="https://townhub.cththemes.com/shop/">Shop</a>
                    </li>
                    <li id="menu-item-1930" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1930">
                        <a href="https://townhub.cththemes.com/news/">News</a>
                    </li>
                    <li id="menu-item-1572" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1572">
                        <a href="#">Pages</a>
                        <ul class="sub-menu">
                            <li id="menu-item-1321" class="ajax menu-item menu-item-type-post_type menu-item-object-page menu-item-1321">
                                <a href="https://townhub.cththemes.com/about/">About</a>
                            </li>
                            <li id="menu-item-1314" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1314">
                                <a href="https://townhub.cththemes.com/contacts/">Contacts</a>
                            </li>
                            <li id="menu-item-6592" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6592">
                                <a href="http://townhub.cththemes.com/author/admin/">Author Single</a>
                            </li>
                            <li id="menu-item-3562" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3562">
                                <a href="https://townhub.cththemes.com/help-center/">Help Center</a>
                            </li>
                            <li id="menu-item-2352" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2352">
                                <a href="https://townhub.cththemes.com/pricing-tables/">Pricing Tables</a>
                            </li>
                            <li id="menu-item-8229" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8229">
                                <a href="https://townhub.cththemes.com/pricing-tables-woo/">Pricing Tables – Woo</a>
                            </li>
                            <li id="menu-item-6574" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-6574">
                                <a href="https://townhub.cththemes.com/integer-sagittis/">Single Post</a>
                            </li>
                            <li id="menu-item-2218" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2218">
                                <a href="http://townhub.cththemes.com/404-page">404</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <!-- #site-navigation -->
        </div>
        <!-- .nav-holder -->
    </header>
    <!--  header end -->
    <!--  wrapper  -->
    <div id="wrapper">
        <!-- Content-->
        <div class="content">
            <div data-elementor-type="wp-post" data-elementor-id="545" class="elementor elementor-545">
                <div class="elementor-inner">
                    <div class="elementor-section-wrap">
                        <section class="elementor-section elementor-top-section elementor-element elementor-element-12d5345 elementor-section-full_width overvi pad-bot-0 pad-top-0 elementor-section-height-default elementor-section-height-default" data-id="12d5345" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-8ddf51e" data-id="8ddf51e" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-2b40ffe hero-section-elementor elementor-widget elementor-widget-hero_section" data-id="2b40ffe" data-element_type="widget" id="sec1" data-widget_type="hero_section.default">
                                                    <div class="elementor-widget-container">
                                                        <section class="scroll-con-sec hero-section elementor-hero-section" data-scrollax-parent="true">
                                                            <div class="hero-bg-wrap hero-bg-absolute">
                                                                <div class="bg" style="background-image:url(https://townhub.cththemes.com/wp-content/uploads/2019/08/1-4.jpg);" data-bg="https://townhub.cththemes.com/wp-content/uploads/2019/08/1-4.jpg" data-scrollax="properties: { translateY: '200px' }"></div>
                                                            </div>
                                                            <div class="overlay" style="opacity:0.5;"></div>
                                                            <div class="hero-section-wrap fl-wrap">
                                                                <div class="container small-container">
                                                                    <div class="intro-item fl-wrap">
                                                                        <span class="section-separator"></span>
                                                                        <div class="bubbles">
                                                                            <h1>Explore Best Places In City</h1>
                                                                        </div>
                                                                        <h3>Find some of the best tips from around the city from our partners and friends.</h3>
                                                                    </div>
                                                                    <div class="main-search-form-wrap clearfix">
                                                                        <!-- main-search-input-tabs-->
                                                                        <div class="main-search-input-tabs  tabs-act fl-wrap tabs-wrapper ltypes-count-4">
                                                                            <ul class="tabs-menu change_bg no-list-style">
                                                                                <li class="lfilter-tabitem current">
                                                                                    <a href="#lfilter-tab-6379" data-bgtab="https://townhub.cththemes.com/wp-content/uploads/2019/08/1-4.jpg">Places</a>
                                                                                </li>
                                                                                <li class="lfilter-tabitem">
                                                                                    <a href="#lfilter-tab-5064" data-bgtab="https://townhub.cththemes.com/wp-content/uploads/2019/08/2-1.jpg">Events</a>
                                                                                </li>
                                                                                <li class="lfilter-tabitem">
                                                                                    <a href="#lfilter-tab-5121" data-bgtab="https://townhub.cththemes.com/wp-content/uploads/2019/08/3-2.jpg">Restaurants</a>
                                                                                </li>
                                                                                <li class="lfilter-tabitem">
                                                                                    <a href="#lfilter-tab-5058" data-bgtab="https://townhub.cththemes.com/wp-content/uploads/2019/08/4-3.jpg">Hotels</a>
                                                                                </li>
                                                                            </ul>
                                                                            <!--tabs -->
                                                                            <div class="tabs-container fl-wrap">
                                                                                <!--tab -->
                                                                                <div class="tab lfilter-tab-6379">
                                                                                    <div id="lfilter-tab-6379" class="tab-content first-tab">
                                                                                        <div class="main-search-input-wrap fl-wrap">
                                                                                            <form role="search" method="get" action="https://townhub.cththemes.com/" class="list-search-hero-form list-search-form-js">
                                                                                                <div class="azp_element filter_form_hero azp-element-azp-krjgkspo8rj">
                                                                                                    <div class="hero-inputs-wrap fl-wrap">
                                                                                                        <div class="azp_element filter_title azp-element-azp-w3dh4rx8g8n filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <label class="flabel-icon">
                                                                                                                    <i class="fal fa-keyboard"></i>
                                                                                                                </label>
                                                                                                                <input type="text" name="search_term" placeholder="What are you looking for?" value="" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="azp_element filter_nearby azp-element-azp-a0iqehqaf4s filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner show-distance-filter nearby-inputs-wrap">
                                                                                                                <div class="nearby-input-wrap nearby-mapbox" id="nearby-wrap62a3d839ce541" data-placeholder="Location">
                                                                                                                    <label class="flabel-icon">
                                                                                                                        <i class="fal fa-location"></i>
                                                                                                                    </label>
                                                                                                                    <input id="auto-place-loc62a3d839ce546" name="location_search" type="text" placeholder="Location" class="qodef-archive-places-search location-input auto-place-loc" value="" />
                                                                                                                    <a href="#" class="get-current-city">
                                                                                                                        <i class="far fa-dot-circle"></i>
                                                                                                                    </a>
                                                                                                                    <span class="autoplace-clear-input">
                                                                                                                                <i class="far fa-times"></i>
                                                                                                                            </span>
                                                                                                                </div>
                                                                                                                <input type="hidden" name="distance" value="10000">
                                                                                                                <input type="hidden" class="auto-place-nearby" name="nearby" value="off">
                                                                                                                <input type="hidden" class="address_lat auto-place-lat" name="address_lat" value="">
                                                                                                                <input type="hidden" class="address_lng auto-place-lng" name="address_lng" value="">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="azp_element filter_cat azp-element-azp-0q20m2t1ljm filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <select data-placeholder="All Categories" class="chosen-select" name="lcats[]">
                                                                                                                    <option value="">All Categories</option>
                                                                                                                    <option value="195">Hotels</option>
                                                                                                                    <option value="382">Events</option>
                                                                                                                    <option value="383">Restaurants</option>
                                                                                                                    <option value="412">Shops</option>
                                                                                                                    <option value="413">Fitness</option>
                                                                                                                    <option value="397">Cars</option>
                                                                                                                    <option value="361">Tour</option>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <button class="main-search-button color2-bg" type="submit">Search <i class="far fa-search"></i>
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--tab end-->
                                                                                <!--tab -->
                                                                                <div class="tab lfilter-tab-5064">
                                                                                    <div id="lfilter-tab-5064" class="tab-content">
                                                                                        <div class="main-search-input-wrap fl-wrap">
                                                                                            <form role="search" method="get" action="https://townhub.cththemes.com/" class="list-search-hero-form list-search-form-js">
                                                                                                <div class="azp_element filter_form_hero azp-element-azp-krjgkspo8rj">
                                                                                                    <div class="hero-inputs-wrap fl-wrap">
                                                                                                        <div class="azp_element filter_title azp-element-azp-w3dh4rx8g8n filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <label class="flabel-icon">
                                                                                                                    <i class="fal fa-handshake-alt"></i>
                                                                                                                </label>
                                                                                                                <input type="text" name="search_term" placeholder="Event Name or Place" value="" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="azp_element filter_loc azp-element-azp-8mx4i6jqrj filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <select data-placeholder="All Cities" class="chosen-select" name="llocs">
                                                                                                                    <option value="">All Cities</option>
                                                                                                                    <option value="bd">Bangladesh</option>
                                                                                                                    <option value="co">Colombia</option>
                                                                                                                    <option value="ec">Ecuador</option>
                                                                                                                    <option value="en">England</option>
                                                                                                                    <option value="fr">France</option>
                                                                                                                    <option value="gr">Greece</option>
                                                                                                                    <option value="in">India</option>
                                                                                                                    <option value="id">Indonesia</option>
                                                                                                                    <option value="it">Italy</option>
                                                                                                                    <option value="ke">Kenya</option>
                                                                                                                    <option value="la">Lao People's Democratic Republic</option>
                                                                                                                    <option value="my">Malaysia</option>
                                                                                                                    <option value="mx">Mexico</option>
                                                                                                                    <option value="np">Nepal</option>
                                                                                                                    <option value="01-new-york">New York</option>
                                                                                                                    <option value="om">Oman</option>
                                                                                                                    <option value="pk">Pakistan</option>
                                                                                                                    <option value="pl">Poland</option>
                                                                                                                    <option value="pt">Portugal</option>
                                                                                                                    <option value="ru">Russia</option>
                                                                                                                    <option value="cs">Serbia and Montenegro</option>
                                                                                                                    <option value="ch">Switzerland</option>
                                                                                                                    <option value="tn">Tunisia</option>
                                                                                                                    <option value="tr">Turkey</option>
                                                                                                                    <option value="tv">Tuvalu</option>
                                                                                                                    <option value="ug">Uganda</option>
                                                                                                                    <option value="ua">Ukraine</option>
                                                                                                                    <option value="us">United States</option>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="azp_element filter_evtdate azp-element-azp-mc2mv865d7q filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <div class="cth-date-picker-wrap esb-field has-icon">
                                                                                                                    <div class="lfield-header">
                                                                                                                                <span class="lfield-icon">
                                                                                                                                    <i class="fal fa-calendar"></i>
                                                                                                                                </span>
                                                                                                                    </div>
                                                                                                                    <div class="cth-date-picker" data-name="event_date" data-format="DD/MM/YYYY" data-default="hide" data-action="" data-postid="" data-selected="general_date" data-placeholder="Event Date"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <button class="main-search-button color2-bg" type="submit">Search <i class="far fa-search"></i>
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <input type="hidden" name="ltype" value="5064">
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--tab end-->
                                                                                <!--tab -->
                                                                                <div class="tab lfilter-tab-5121">
                                                                                    <div id="lfilter-tab-5121" class="tab-content">
                                                                                        <div class="main-search-input-wrap fl-wrap">
                                                                                            <form role="search" method="get" action="https://townhub.cththemes.com/" class="list-search-hero-form list-search-form-js">
                                                                                                <div class="azp_element filter_form_hero azp-element-azp-krjgkspo8rj">
                                                                                                    <div class="hero-inputs-wrap fl-wrap">
                                                                                                        <div class="azp_element filter_title azp-element-azp-w3dh4rx8g8n filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <label class="flabel-icon">
                                                                                                                    <i class="fal fa-cheeseburger"></i>
                                                                                                                </label>
                                                                                                                <input type="text" name="search_term" placeholder="Restaurant Name" value="" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="azp_element azp_filter_date azp-element-azp-zy0mgn8065c filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <div class="cth-date-picker-wrap esb-field has-icon">
                                                                                                                    <div class="lfield-header">
                                                                                                                                <span class="lfield-icon">
                                                                                                                                    <i class="fal fa-calendar"></i>
                                                                                                                                </span>
                                                                                                                        <label class="lfield-label">Date and Time</label>
                                                                                                                    </div>
                                                                                                                    <div class="cth-date-picker" data-name="checkin" data-format="DD/MM/YYYY" data-default="hide" data-action="" data-postid="" data-selected="general_date" data-placeholder="Select Date"></div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="azp_element filter_persons azp-element-azp-te6i16usgo filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <label class="flabel-icon">
                                                                                                                    <i class="fal fa-user-friends"></i>
                                                                                                                </label>
                                                                                                                <input type="text" name="adults" placeholder="Persons" value="" pattern="[0-9]*" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <button class="main-search-button color2-bg" type="submit">Search <i class="far fa-search"></i>
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <input type="hidden" name="ltype" value="5121">
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--tab end-->
                                                                                <!--tab -->
                                                                                <div class="tab lfilter-tab-5058">
                                                                                    <div id="lfilter-tab-5058" class="tab-content">
                                                                                        <div class="main-search-input-wrap fl-wrap">
                                                                                            <form role="search" method="get" action="https://townhub.cththemes.com/" class="list-search-hero-form list-search-form-js">
                                                                                                <div class="azp_element filter_form_hero azp-element-azp-krjgkspo8rj">
                                                                                                    <div class="hero-inputs-wrap fl-wrap">
                                                                                                        <div class="azp_element filter_title azp-element-azp-w3dh4rx8g8n filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <label class="flabel-icon">
                                                                                                                    <i class="fal fa-cheeseburger"></i>
                                                                                                                </label>
                                                                                                                <input type="text" name="search_term" placeholder="Hotel Name" value="" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="azp_element filter_persons azp-element-azp-3jg3n5vbbfy filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <label class="flabel-icon">
                                                                                                                    <i class="fal fa-user-friends"></i>
                                                                                                                </label>
                                                                                                                <input type="text" name="adults" placeholder="Persons" value="" pattern="[0-9]*" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="azp_element azp_filter_date azp-element-azp-cawrydu4vfm filter-gid-item filter-gid-wid-4">
                                                                                                            <div class="filter-item-inner">
                                                                                                                <div class="cth-daterange-picker" data-name="checkin" data-name2="checkout" data-format="DD/MM/YYYY" data-default="current" data-label="Date In Out" data-icon="fal fa-calendar" data-selected="slot_date"></div>
                                                                                                                <!-- <span class="clear-singleinput"><i class="fal fa-times"></i></span> -->
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <button class="main-search-button color2-bg" type="submit">Search <i class="far fa-search"></i>
                                                                                                        </button>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <input type="hidden" name="ltype" value="5058">
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--tab end-->
                                                                            </div>
                                                                            <!--tabs end-->
                                                                        </div>
                                                                        <!-- main-search-input-tabs end-->
                                                                    </div>
                                                                    <div class="hero-categories fl-wrap">
                                                                        <h4 class="hero-categories_title">Just looking around ? Use quick search by category :</h4>
                                                                        <ul class="no-list-style">
                                                                            <li>
                                                                                <a href="https://townhub.cththemes.com/listing_cat/01-restaurants/" class="hero-cat-link hero-cat-red-bg">
                                                                                    <i class="far fa-cheeseburger"></i>
                                                                                    <span>Restaurants</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="https://townhub.cththemes.com/listing_cat/02-hotels/" class="hero-cat-link hero-cat-yellow-bg">
                                                                                    <i class="far fa-bed"></i>
                                                                                    <span>Hotels</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="https://townhub.cththemes.com/listing_cat/03-events/" class="hero-cat-link hero-cat-purp-bg">
                                                                                    <i class="fas fa-cocktail"></i>
                                                                                    <span>Events</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="https://townhub.cththemes.com/listing_cat/04-fitness/" class="hero-cat-link hero-cat-blue-bg">
                                                                                    <i class="far fa-dumbbell"></i>
                                                                                    <span>Fitness</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="https://townhub.cththemes.com/listing_cat/05-shops/" class="hero-cat-link hero-cat-green-bg">
                                                                                    <i class="fal fa-shopping-bag"></i>
                                                                                    <span>Shops</span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--<div class="bubble-bg"></div> -->
                                                            <div class="header-sec-link">
                                                                <a href="#sec2" class="custom-scroll-link">
                                                                    <i class="fal fa-angle-double-down"></i>
                                                                </a>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="elementor-section elementor-top-section elementor-element elementor-element-3620242 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="3620242" data-element_type="section" id="sec2">
                            <div class="elementor-container elementor-column-gap-no">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-05188aa" data-id="05188aa" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <section class="elementor-section elementor-inner-section elementor-element elementor-element-1fe2bd2 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1fe2bd2" data-element_type="section">
                                                    <div class="elementor-container elementor-column-gap-default">
                                                        <div class="elementor-row">
                                                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-deebded" data-id="deebded" data-element_type="column">
                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                        <div class="elementor-element elementor-element-6362c6a elementor-widget elementor-widget-section_title" data-id="6362c6a" data-element_type="widget" data-widget_type="section_title.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="section-title section-title-dk-blue fl-wrap">
                                                                                    <h2>
                                                                                        <span>The Latest Listings</span>
                                                                                    </h2>
                                                                                    <div class="section-subtitle">Newest Listings</div>
                                                                                    <span class="section-separator section-separator-dk-blue"></span>
                                                                                    <p>Mauris ac maximus neque. Nam in mauris quis libero sodales eleifend. Morbi varius, nulla sit amet rutrum elementum.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <section class="elementor-section elementor-inner-section elementor-element elementor-element-d96e4fe elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="d96e4fe" data-element_type="section">
                                                    <div class="elementor-container elementor-column-gap-no">
                                                        <div class="elementor-row">
                                                            <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-7270b9f" data-id="7270b9f" data-element_type="column">
                                                                <div class="elementor-column-wrap elementor-element-populated">
                                                                    <div class="elementor-widget-wrap">
                                                                        <div class="elementor-element elementor-element-dbd6240 elementor-widget elementor-widget-listings_slider" data-id="dbd6240" data-element_type="widget" data-widget_type="listings_slider.default">
                                                                            <div class="elementor-widget-container">
                                                                                <!-- carousel -->
                                                                                <div class="listing-slider-wrap fl-wrap">
                                                                                    <div class="listing-slider fl-wrap listing-slider-editor-col-4">
                                                                                        <div class="swiper-container" data-options='{"simulateTouch":false,"slidesPerView":4,"breakpoints":{"850":{"slidesPerView":1},"1270":{"slidesPerView":2},"1650":{"slidesPerView":3},"2560":{"slidesPerView":4}},"centeredSlides":true}'>
                                                                                            <div class="swiper-wrapper">
                                                                                                <!--  swiper-slide  -->
                                                                                                <div class="swiper-slide">
                                                                                                    <div class="listing-slider-item fl-wrap">
                                                                                                        <!-- listing-item  -->
                                                                                                        <div class="listing-item listing_carditem">
                                                                                                            <article class="geodir-category-listing fl-wrap">
                                                                                                                <div class="geodir-category-img">
                                                                                                                    <div class="geodir-js-favorite_btn">
                                                                                                                        <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                                            <i class="fal fa-heart"></i>
                                                                                                                            <span>Save</span>
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                    <a href="https://townhub.cththemes.com/listing/shop-in-aurora-center/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                                        <img width="382" height="252" class="respimg swiper-lazy" alt="" loading="lazy" data-src="https://dummyimage.com/270x270/3f9ee2/ffffff.jpg&text=AF" />
                                                                                                                    </a>
                                                                                                                    <div class="geodir_status_date lstatus-opening">
                                                                                                                        <i class="fal fa-lock-open"></i>Now Open
                                                                                                                    </div>
                                                                                                                    <div class="geodir-category-opt dis-flex flw-wrap">
                                                                                                                        <div class="geodir-category-opt_title">
                                                                                                                            <h4>
                                                                                                                                <a href="https://townhub.cththemes.com/listing/shop-in-aurora-center/">Ahmad Food Channel</a>
                                                                                                                            </h4>
                                                                                                                            <div class="geodir-category-location">
                                                                                                                                <a href="https://www.google.com/maps/search/?api=1&query=40.701594124269405,-73.88509092852472" target="_blank">
                                                                                                                                    <i class="fas fa-map-marker-alt"></i>Miri, Sarawak</a>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="listing-rating-count-wrap flex-items-center">
                                                                                                                            <div class="review-score">4.3</div>
                                                                                                                            <div class="review-details">
                                                                                                                                <div class="listing-rating card-popup-rainingvis" data-rating="4.3"></div>
                                                                                                                                <div class="reviews-count">1 comment</div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="listing_carditem_footer fl-wrap dis-flex">
                                                                                                                            <div class="listing-cats-wrap dis-flex">
                                                                                                                                <a href="https://townhub.cththemes.com/listing_cat/05-shops/" class="listing-item-category-wrap flex-items-center">
                                                                                                                                    <div class="listing-item-category green-bg">
                                                                                                                                        <i class="fal fa-shopping-bag"></i>
                                                                                                                                    </div>
                                                                                                                                    <span>Shops</span>
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                            <div class="post-author flex-items-center">
                                                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/11/cth_avatar.jpeg" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/11/cth_avatar.jpeg">
                                                                                                                                    <span class="avatar-info">By , CTHthemes</span>
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </article>
                                                                                                        </div>
                                                                                                        <!-- listing-item end -->
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--  swiper-slide end  -->
                                                                                                <!--  swiper-slide  -->
                                                                                                <div class="swiper-slide">
                                                                                                    <div class="listing-slider-item fl-wrap">
                                                                                                        <!-- listing-item  -->
                                                                                                        <div class="listing-item listing_carditem">
                                                                                                            <article class="geodir-category-listing fl-wrap">
                                                                                                                <div class="geodir-category-img">
                                                                                                                    <div class="geodir-js-favorite_btn">
                                                                                                                        <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                                            <i class="fal fa-heart"></i>
                                                                                                                            <span>Save</span>
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                    <a href="https://townhub.cththemes.com/listing/city-street-coffee/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                                        <img width="382" height="252" class="respimg swiper-lazy" alt="" loading="lazy" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14459/5c74c15a2bc51-1551155546.jpeg" />
                                                                                                                    </a>
                                                                                                                    <div class="geodir_status_date lstatus-closed">
                                                                                                                        <i class="fal fa-lock"></i>Now Closed
                                                                                                                    </div>
                                                                                                                    <div class="geodir-category-opt dis-flex flw-wrap">
                                                                                                                        <div class="geodir-category-opt_title">
                                                                                                                            <h4>
                                                                                                                                <a href="https://townhub.cththemes.com/listing/city-street-coffee/">AJ Beauty</a>
                                                                                                                                <span class="verified-badge">
                                                                                                                                            <i class="fal fa-check"></i>
                                                                                                                                        </span>
                                                                                                                            </h4>
                                                                                                                            <div class="geodir-category-location">
                                                                                                                                <a href="https://www.google.com/maps/search/?api=1&query=40.830849,-73.89349299999998" target="_blank">
                                                                                                                                    <i class="fas fa-map-marker-alt"></i>KUCHING, SARAWAK</a>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="listing-rating-count-wrap flex-items-center">
                                                                                                                            <div class="review-score">4.4</div>
                                                                                                                            <div class="review-details">
                                                                                                                                <div class="listing-rating card-popup-rainingvis" data-rating="4.4"></div>
                                                                                                                                <div class="reviews-count">4 comments</div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="listing_carditem_footer fl-wrap dis-flex">
                                                                                                                            <div class="listing-cats-wrap dis-flex">
                                                                                                                                <a href="https://townhub.cththemes.com/listing_cat/07-cars/" class="listing-item-category-wrap flex-items-center">
                                                                                                                                    <div class="listing-item-category red-bg">
                                                                                                                                        <i class="fas fa-car"></i>
                                                                                                                                    </div>
                                                                                                                                    <span>Cars</span>
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                            <div class="post-author flex-items-center">
                                                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/11/cth_avatar.jpeg" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/11/cth_avatar.jpeg">
                                                                                                                                    <span class="avatar-info">By , CTHthemes</span>
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </article>
                                                                                                        </div>
                                                                                                        <!-- listing-item end -->
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--  swiper-slide end  -->
                                                                                                <!--  swiper-slide  -->
                                                                                                <div class="swiper-slide">
                                                                                                    <div class="listing-slider-item fl-wrap">
                                                                                                        <!-- listing-item  -->
                                                                                                        <div class="listing-item listing_carditem">
                                                                                                            <article class="geodir-category-listing fl-wrap">
                                                                                                                <div class="geodir-category-img">
                                                                                                                    <div class="geodir-js-favorite_btn">
                                                                                                                        <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                                            <i class="fal fa-heart"></i>
                                                                                                                            <span>Save</span>
                                                                                                                        </a>
                                                                                                                    </div>
                                                                                                                    <div class="listing-featured">Featured</div>
                                                                                                                    <a href="https://townhub.cththemes.com/listing/a-consortium-of-music/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                                        <img width="382" height="252" class="respimg swiper-lazy" alt="" loading="lazy" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14653/5d2ebb28758e6-1563343656.jpeg" />
                                                                                                                    </a>
                                                                                                                    <div class="geodir_status_date lstatus-opening">
                                                                                                                        <i class="fal fa-clock"></i>July 3, 2022 8:30 am
                                                                                                                    </div>
                                                                                                                    <div class="geodir-category-opt dis-flex flw-wrap">
                                                                                                                        <div class="geodir-category-opt_title">
                                                                                                                            <h4>
                                                                                                                                <a href="https://townhub.cththemes.com/listing/a-consortium-of-music/">AJ Wave Marketing</a>
                                                                                                                                <span class="verified-badge">
                                                                                                                                            <i class="fal fa-check"></i>
                                                                                                                                        </span>
                                                                                                                            </h4>
                                                                                                                            <div class="geodir-category-location">
                                                                                                                                <a href="https://www.google.com/maps/search/?api=1&query=40.78708237136644,-73.97548763779922" target="_blank">
                                                                                                                                    <i class="fas fa-map-marker-alt"></i>-</a>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="listing-rating-count-wrap flex-items-center">
                                                                                                                            <div class="review-score">4.0</div>
                                                                                                                            <div class="review-details">
                                                                                                                                <div class="listing-rating card-popup-rainingvis" data-rating="4.0"></div>
                                                                                                                                <div class="reviews-count">1 comment</div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="listing_carditem_footer fl-wrap dis-flex">
                                                                                                                            <div class="listing-cats-wrap dis-flex">
                                                                                                                                <a href="https://townhub.cththemes.com/listing_cat/03-events/" class="listing-item-category-wrap flex-items-center">
                                                                                                                                    <div class="listing-item-category purp-bg">
                                                                                                                                        <i class="fas fa-cocktail"></i>
                                                                                                                                    </div>
                                                                                                                                    <span>Events</span>
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                            <div class="post-author flex-items-center">
                                                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/11/cth_avatar.jpeg" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/11/cth_avatar.jpeg">
                                                                                                                                    <span class="avatar-info">By , CTHthemes</span>
                                                                                                                                </a>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </article>
                                                                                                        </div>
                                                                                                        <!-- listing-item end -->
                                                                                                    </div>
                                                                                                </div>
                                                                                                <!--  swiper-slide end  -->
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="listing-carousel-button listing-carousel-button-next2">
                                                                                            <i class="fas fa-caret-right"></i>
                                                                                        </div>
                                                                                        <div class="listing-carousel-button listing-carousel-button-prev2">
                                                                                            <i class="fas fa-caret-left"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="tc-pagination_wrap">
                                                                                        <div class="tc-pagination2"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <!--  carousel end-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="elementor-section elementor-top-section elementor-element elementor-element-cd9d684 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="cd9d684" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-cb63b6d" data-id="cb63b6d" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-9520213 elementor-widget elementor-widget-section_title" data-id="9520213" data-element_type="widget" data-widget_type="section_title.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="section-title section-title-dk-blue fl-wrap">
                                                            <h2>
                                                                <span>Most Popular Palces</span>
                                                            </h2>
                                                            <div class="section-subtitle">Best Listings</div>
                                                            <span class="section-separator section-separator-dk-blue"></span>
                                                            <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus, finibus tellus ut, convallis eros sollicitudin turpis.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-510bf57 elementor-widget elementor-widget-listings_grid_new" data-id="510bf57" data-element_type="widget" data-widget_type="listings_grid_new.default">
                                                    <div class="elementor-widget-container">
                                                        <!-- carousel -->
                                                        <div class="listings-grid-wrap clearfix three-cols">
                                                            <!-- list-main-wrap-->
                                                            <div class="list-main-wrap fl-wrap card-listing cthiso-isotope-wrapper">
                                                                <div class="container">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <!-- filter  -->
                                                                            <div class="cthiso-filters fl-wrap">
                                                                                <a href="#" class="cthiso-filter cthiso-filter-active" data-filter="*">All Categories</a>
                                                                                <a href="#" class="cthiso-filter" data-filter=".listing_cat-01-restaurants">Restaurants</a>
                                                                                <a href="#" class="cthiso-filter" data-filter=".listing_cat-02-hotels">Hotels</a>
                                                                                <a href="#" class="cthiso-filter" data-filter=".listing_cat-03-events">Events</a>
                                                                                <a href="#" class="cthiso-filter" data-filter=".listing_cat-04-fitness">Fitness</a>
                                                                            </div>
                                                                            <!-- filter end -->
                                                                            <div class="cthiso-items cthiso-small-pad cthiso-three-cols clearfix cthiso-flex">
                                                                                <div class="cthiso-sizer"></div>
                                                                                <!-- listing-item -->
                                                                                <div class="cthiso-item listing-item-loop post-5122 listing type-listing status-publish has-post-thumbnail hentry listing_cat-01-restaurants listing_feature-elevator-in-building listing_feature-free-parking listing_feature-free-wi-fi listing_feature-mini-bar listing_feature-pet-friendly listing_location-en listing_location-05-london listing_tag-hostel listing_tag-hotel listing_tag-parking listing_tag-restourant listing_tag-room listing_tag-spa" data-postid="5122">
                                                                                    <article class="geodir-category-listing fl-wrap">
                                                                                        <div class="azp_element preview_listing azp-element-azp-sgfq927d1s geodir-category-img">
                                                                                            <div class="geodir-js-favorite_btn">
                                                                                                <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                    <i class="fal fa-heart"></i>
                                                                                                    <span>Save</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="listing-featured">Featured</div>
                                                                                            <a href="https://townhub.cththemes.com/listing/the-goggi-restaurant/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                <img width="382" height="252" style="height:252px;" class="respimg lazy" alt="" loading="lazy" data-src="https://ukuyacommunity.s3.amazonaws.com/v2/organization/logo/y3fpRDgVUyMCIWtJ2ElPTsfYXDPEZ0qo.jpg" src="https://ukuyacommunity.s3.amazonaws.com/v2/organization/logo/y3fpRDgVUyMCIWtJ2ElPTsfYXDPEZ0qo.jpg" />
                                                                                                <div class="overlay"></div>
                                                                                            </a>
                                                                                            <div class="listing-avatar">
                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://ukuyacommunity.s3.amazonaws.com/v2/organization/logo/y3fpRDgVUyMCIWtJ2ElPTsfYXDPEZ0qo.jpg" data-lazy="https://ukuyacommunity.s3.amazonaws.com/v2/organization/logo/y3fpRDgVUyMCIWtJ2ElPTsfYXDPEZ0qo.jpg">
                                                                                                </a>
                                                                                                <span class="avatar-tooltip lpre-avatar">Added By <strong>Townhub</strong>
                                                                                                        </span>
                                                                                            </div>
                                                                                            <div class="lcard-saleoff">
                                                                                                <div class="saleoff-inner">Sale 20%</div>
                                                                                            </div>
                                                                                            <div class="geodir_status_date lstatus-closed">
                                                                                                <i class="fal fa-lock"></i>Now Closed
                                                                                            </div>
                                                                                            <div class="geodir-category-opt clearfix">
                                                                                                <div class="listing-rating-count-wrap flex-items-center">
                                                                                                    <div class="review-score">4.4</div>
                                                                                                    <div class="review-details">
                                                                                                        <div class="listing-rating card-popup-rainingvis" data-rating="4.4"></div>
                                                                                                        <div class="reviews-count">2 comments</div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="lcfields-wrap dis-flex"></div>
                                                                                        </div>
                                                                                        <div class="azp_element preview_listing_content azp-element-azp-e49ww3xidf9 geodir-category-content">
                                                                                            <div class="geodir-category-content-title fl-wrap">
                                                                                                <div class="geodir-category-content-title-item">
                                                                                                    <h3 class="title-sin_map">
                                                                                                        <a href="https://townhub.cththemes.com/listing/the-goggi-restaurant/">ALVERNIA AGNES ENTERPRISE</a>
                                                                                                    </h3>
                                                                                                    <div class="geodir-category-location fl-wrap">
                                                                                                        <a href="https://www.google.com/maps/search/?api=1&query=40.73116967880068,-74.06466809101403" class="map-item" target="_blank">
                                                                                                            <i class="fas fa-map-marker-alt"></i>W 85th St, New York, NY, USA </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-text fl-wrap"> Sed interdum metus at nisi tempor laoreet. Integer ... <div class="lcfields-wrap dis-flex"></div>
                                                                                                <div class="facilities-list fl-wrap dis-flex flw-wrap">
                                                                                                    <div class="facilities-list-title">Facilities:</div>
                                                                                                    <ul class="no-list-style mrg-0 dis-inflex">
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Elevator in building">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/elevator-in-building/">
                                                                                                                <i class="fal fa-rocket"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Free Parking">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/free-parking/">
                                                                                                                <i class="fal fa-parking"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Free Wi Fi">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/free-wi-fi/">
                                                                                                                <i class="fal fa-wifi"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Mini Bar">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/mini-bar/">
                                                                                                                <i class="fal fa-glass-martini"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="lcard-price">Price: <strong>$&nbsp;100.00</strong>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-footer fl-wrap dis-flex">
                                                                                                <div class="listing-cats-wrap dis-flex">
                                                                                                    <a href="https://townhub.cththemes.com/listing_cat/01-restaurants/" class="listing-item-category-wrap flex-items-center">
                                                                                                        <div class="listing-item-category red-bg">
                                                                                                            <i class="far fa-cheeseburger"></i>
                                                                                                        </div>
                                                                                                        <span>Restaurants</span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="geodir-opt-list dis-flex">
                                                                                                    <ul class="no-list-style">
                                                                                                        <li class="lcard-bot-infos">
                                                                                                            <a href="#" class="show_gcc">
                                                                                                                <i class="fal fa-envelope"></i>
                                                                                                                <span class="geodir-opt-tooltip">Contact Info</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="lcard-bot-gallery">
                                                                                                            <div class="dynamic-gal gdop-list-link" data-dynamicPath='[{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/12-1.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/24.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/21-1.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/47.jpg"}]'>
                                                                                                                <i class="fal fa-search-plus"></i>
                                                                                                                <span class="geodir-opt-tooltip">Gallery</span>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="geodir-category_contacts">
                                                                                                    <div class="close_gcc">
                                                                                                        <i class="fal fa-times-circle"></i>
                                                                                                    </div>
                                                                                                    <ul class="no-list-style">
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-phone"></i> Call : </span>
                                                                                                            <a href="tel:+7(123)987654">+60198343778</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-envelope"></i> Write : </span>
                                                                                                            <a href="/cdn-cgi/l/email-protection#a9d0c6dcdbc4c8c0c5e9cdc6c4c8c0c787cac6c4">
                                                                                                                <span class="__cf_email__" data-cfemail="354c5a404758545c5975515a58545c5b1b565a58">[email&#160;protected]</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-link"></i>
                                                                                                                    </span>
                                                                                                            <a href="https://cththemes.com">Visit website</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </article>
                                                                                </div>
                                                                                <!-- listing-item end-->
                                                                                <!-- listing-item -->
                                                                                <div class="cthiso-item listing-item-loop post-1739 listing type-listing status-publish has-post-thumbnail hentry listing_cat-01-restaurants listing_feature-breakfast listing_feature-free-parking listing_feature-free-wi-fi listing_feature-restaurant-inside listing_location-fr listing_location-02-paris listing_tag-fitness listing_tag-gym listing_tag-indoor listing_tag-outdoor listing_tag-running" data-postid="1739">
                                                                                    <article class="geodir-category-listing fl-wrap">
                                                                                        <div class="azp_element preview_listing azp-element-azp-sgfq927d1s geodir-category-img">
                                                                                            <div class="geodir-js-favorite_btn">
                                                                                                <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                    <i class="fal fa-heart"></i>
                                                                                                    <span>Save</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <a href="https://townhub.cththemes.com/listing/iconic-cafe-in-manhattan/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                <img width="382" height="252" style="height:252px;" class="respimg lazy" alt="" loading="lazy" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14462/5c7b3817e716c-1551579159.jpg" src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14462/5c7b3817e716c-1551579159.jpg" />
                                                                                                <div class="overlay"></div>
                                                                                            </a>
                                                                                            <div class="listing-avatar">
                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14462/5c7b3817e716c-1551579159.jpg" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14462/5c7b3817e716c-1551579159.jpg" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14462/5c7b3817e716c-1551579159.jpg">
                                                                                                </a>
                                                                                                <span class="avatar-tooltip lpre-avatar">Added By <strong>CTHthemes</strong>
                                                                                                        </span>
                                                                                            </div>
                                                                                            <div class="geodir_status_date lstatus-closed">
                                                                                                <i class="fal fa-lock"></i>Now Closed
                                                                                            </div>
                                                                                            <div class="geodir-category-opt clearfix">
                                                                                                <div class="listing-rating-count-wrap flex-items-center">
                                                                                                    <div class="review-score">4.3</div>
                                                                                                    <div class="review-details">
                                                                                                        <div class="listing-rating card-popup-rainingvis" data-rating="4.3"></div>
                                                                                                        <div class="reviews-count">2 comments</div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="lcfields-wrap dis-flex"></div>
                                                                                        </div>
                                                                                        <div class="azp_element preview_listing_content azp-element-azp-e49ww3xidf9 geodir-category-content">
                                                                                            <div class="geodir-category-content-title fl-wrap">
                                                                                                <div class="geodir-category-content-title-item">
                                                                                                    <h3 class="title-sin_map">
                                                                                                        <a href="https://townhub.cththemes.com/listing/iconic-cafe-in-manhattan/">Amy Yin Enterprise</a>
                                                                                                    </h3>
                                                                                                    <div class="geodir-category-location fl-wrap">
                                                                                                        <a href="https://www.google.com/maps/search/?api=1&query=40.7127736,-73.9850682" class="map-item" target="_blank">
                                                                                                            <i class="fas fa-map-marker-alt"></i>kuching, sarawak</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-text fl-wrap"> Sed interdum metus at nisi tempor laoreet. Integer ... <div class="lcfields-wrap dis-flex"></div>
                                                                                                <div class="facilities-list fl-wrap dis-flex flw-wrap">
                                                                                                    <div class="facilities-list-title">Facilities:</div>
                                                                                                    <ul class="no-list-style mrg-0 dis-inflex">
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Breakfast">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/breakfast/">
                                                                                                                <i class="fal fa-concierge-bell"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Free Parking">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/free-parking/">
                                                                                                                <i class="fal fa-parking"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Free Wi Fi">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/free-wi-fi/">
                                                                                                                <i class="fal fa-wifi"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Restaurant Inside">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/restaurant-inside/">
                                                                                                                <i class="fal fa-utensils"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="lcard-price">Price: <strong>$&nbsp;100.00</strong>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-footer fl-wrap dis-flex">
                                                                                                <div class="listing-cats-wrap dis-flex">
                                                                                                    <a href="https://townhub.cththemes.com/listing_cat/01-restaurants/" class="listing-item-category-wrap flex-items-center">
                                                                                                        <div class="listing-item-category red-bg">
                                                                                                            <i class="far fa-cheeseburger"></i>
                                                                                                        </div>
                                                                                                        <span>Restaurants</span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="geodir-opt-list dis-flex">
                                                                                                    <ul class="no-list-style">
                                                                                                        <li class="lcard-bot-infos">
                                                                                                            <a href="#" class="show_gcc">
                                                                                                                <i class="fal fa-envelope"></i>
                                                                                                                <span class="geodir-opt-tooltip">Contact Info</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="lcard-bot-gallery">
                                                                                                            <div class="dynamic-gal gdop-list-link" data-dynamicPath='[{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/12-1.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/24.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/21-1.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/47.jpg"}]'>
                                                                                                                <i class="fal fa-search-plus"></i>
                                                                                                                <span class="geodir-opt-tooltip">Gallery</span>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="geodir-category_contacts">
                                                                                                    <div class="close_gcc">
                                                                                                        <i class="fal fa-times-circle"></i>
                                                                                                    </div>
                                                                                                    <ul class="no-list-style">
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-phone"></i> Call : </span>
                                                                                                            <a href="tel:+7(123)987654">+01119879039</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-envelope"></i> Write : </span>
                                                                                                            <a href="/cdn-cgi/l/email-protection#8af3e5fff8e7ebe3e6caeee5e7ebe3e4a4e9e5e7">
                                                                                                                <span class="__cf_email__" data-cfemail="a6dfc9d3d4cbc7cfcae6c2c9cbc7cfc888c5c9cb">[email&#160;protected]</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-link"></i>
                                                                                                                    </span>
                                                                                                            <a href="https://cththemes.com">Visit website</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </article>
                                                                                </div>
                                                                                <!-- listing-item end-->
                                                                                <!-- listing-item -->
                                                                                <div class="cthiso-item listing-item-loop post-1744 listing type-listing status-publish has-post-thumbnail hentry listing_cat-02-hotels listing_feature-air-conditioned listing_feature-airport-shuttle listing_feature-breakfast listing_feature-elevator-in-building listing_feature-free-parking listing_feature-free-wi-fi listing_feature-pet-friendly listing_feature-restaurant-inside listing_location-it listing_location-04-rome" data-postid="1744">
                                                                                    <article class="geodir-category-listing fl-wrap">
                                                                                        <div class="azp_element preview_listing azp-element-azp-vfrua9v41ol geodir-category-img">
                                                                                            <div class="geodir-js-favorite_btn">
                                                                                                <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                    <i class="fal fa-heart"></i>
                                                                                                    <span>Save</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="listing-featured">Featured</div>
                                                                                            <a href="https://townhub.cththemes.com/listing/luxury-restourant/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                <img width="382" height="252" style="height:252px;" class="respimg lazy" alt="" loading="lazy" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14522/5cbbe630b6fbc-1555818032.png" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14522/5cbbe630b6fbc-1555818032.png" src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14522/5cbbe630b6fbc-1555818032.png" />
                                                                                                <div class="overlay"></div>
                                                                                            </a>
                                                                                            <div class="listing-avatar">
                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14522/5cbbe630b6fbc-1555818032.png" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14522/5cbbe630b6fbc-1555818032.png">
                                                                                                </a>
                                                                                                <span class="avatar-tooltip lpre-avatar">Added By <strong>CTHthemes</strong>
                                                                                                        </span>
                                                                                            </div>
                                                                                            <div class="geodir_status_date lstatus-opening">
                                                                                                <i class="fal fa-lock-open"></i>Now Open
                                                                                            </div>
                                                                                            <div class="geodir-category-opt clearfix">
                                                                                                <div class="listing-rating-count-wrap flex-items-center">
                                                                                                    <div class="review-score">4.0</div>
                                                                                                    <div class="review-details">
                                                                                                        <div class="listing-rating card-popup-rainingvis" data-rating="4.0"></div>
                                                                                                        <div class="reviews-count">1 comment</div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="lcfields-wrap dis-flex"></div>
                                                                                        </div>
                                                                                        <div class="azp_element preview_listing_content azp-element-azp-0mypklfokr6 geodir-category-content">
                                                                                            <div class="geodir-category-content-title fl-wrap">
                                                                                                <div class="geodir-category-content-title-item">
                                                                                                    <h3 class="title-sin_map">
                                                                                                        <a href="https://townhub.cththemes.com/listing/luxury-restourant/">ANJAAD BORNEO SDN BHD</a>
                                                                                                        <span class="verified-badge">
                                                                                                                    <i class="fal fa-check"></i>
                                                                                                                </span>
                                                                                                    </h3>
                                                                                                    <div class="geodir-category-location fl-wrap">
                                                                                                        <a href="https://www.google.com/maps/search/?api=1&query=40.7172387,-74.04772960000003" class="map-item" target="_blank">
                                                                                                            <i class="fas fa-map-marker-alt"></i>70 Bright St, Jersey City, NJ, USA </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-text fl-wrap"> Sed interdum metus at nisi tempor laoreet. Integer ... <div class="lcfields-wrap dis-flex"></div>
                                                                                                <div class="facilities-list fl-wrap dis-flex flw-wrap">
                                                                                                    <div class="facilities-list-title">Facilities:</div>
                                                                                                    <ul class="no-list-style mrg-0 dis-inflex">
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Air Conditioned">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/air-conditioned/">
                                                                                                                <i class="fal fa-snowflake"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Airport Shuttle">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/airport-shuttle/">
                                                                                                                <i class="fal fa-plane"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Breakfast">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/breakfast/">
                                                                                                                <i class="fal fa-concierge-bell"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Elevator in building">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/elevator-in-building/">
                                                                                                                <i class="fal fa-rocket"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="lcard-price">Price: <strong>$&nbsp;122.00</strong>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-footer fl-wrap dis-flex">
                                                                                                <div class="listing-cats-wrap dis-flex">
                                                                                                    <a href="https://townhub.cththemes.com/listing_cat/02-hotels/" class="listing-item-category-wrap flex-items-center">
                                                                                                        <div class="listing-item-category yellow-bg">
                                                                                                            <i class="far fa-bed"></i>
                                                                                                        </div>
                                                                                                        <span>Hotels</span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="price-level geodir-category_price dis-flex">
                                                                                                    <span class="price-level-item" data-pricerating="4"></span>
                                                                                                    <span class="price-name-tooltip">Ultra High</span>
                                                                                                </div>
                                                                                                <div class="geodir-opt-list dis-flex">
                                                                                                    <ul class="no-list-style">
                                                                                                        <li class="lcard-bot-infos">
                                                                                                            <a href="#" class="show_gcc">
                                                                                                                <i class="fal fa-envelope"></i>
                                                                                                                <span class="geodir-opt-tooltip">Contact Info</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="geodir-category_contacts">
                                                                                                    <div class="close_gcc">
                                                                                                        <i class="fal fa-times-circle"></i>
                                                                                                    </div>
                                                                                                    <ul class="no-list-style">
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-phone"></i> Call : </span>
                                                                                                            <a href="tel:+7(123)987654">+60198343778</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-envelope"></i> Write : </span>
                                                                                                            <a href="/cdn-cgi/l/email-protection#dda4b2a8afb0bcb4b19db9b2b0bcb4b3f3beb2b0">
                                                                                                                <span class="__cf_email__" data-cfemail="0871677d7a65696164486c6765696166266b6765">[email&#160;protected]</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-link"></i>
                                                                                                                    </span>
                                                                                                            <a href="https://cththemes.com">Visit website</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </article>
                                                                                </div>
                                                                                <!-- listing-item end-->
                                                                                <!-- listing-item -->
                                                                                <div class="cthiso-item listing-item-loop post-1886 listing type-listing status-publish has-post-thumbnail hentry listing_cat-02-hotels listing_feature-breakfast listing_feature-free-parking listing_feature-free-wi-fi listing_feature-restaurant-inside listing_location-01-new-york listing_tag-hostel listing_tag-hotel listing_tag-parking listing_tag-restaurant listing_tag-room listing_tag-spa" data-postid="1886">
                                                                                    <article class="geodir-category-listing fl-wrap">
                                                                                        <div class="azp_element preview_listing azp-element-azp-vfrua9v41ol geodir-category-img">
                                                                                            <div class="geodir-js-favorite_btn">
                                                                                                <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                    <i class="fal fa-heart"></i>
                                                                                                    <span>Save</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="listing-featured">Featured</div>
                                                                                            <a href="https://townhub.cththemes.com/listing/monteplaza-hotel/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                <img width="382" height="252" style="height:252px;" class="respimg lazy" alt="" loading="lazy" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14468/5c74ba1ea8840-1551153694.jpeg" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14468/5c74ba1ea8840-1551153694.jpeg" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                                                <div class="overlay"></div>
                                                                                            </a>
                                                                                            <div class="listing-avatar">
                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14468/5c74ba1ea8840-1551153694.jpeg" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14468/5c74ba1ea8840-1551153694.jpeg">
                                                                                                </a>
                                                                                                <span class="avatar-tooltip lpre-avatar">Added By <strong>CTHthemes</strong>
                                                                                                        </span>
                                                                                            </div>
                                                                                            <div class="geodir_status_date lstatus-opening">
                                                                                                <i class="fal fa-lock-open"></i>Now Open
                                                                                            </div>
                                                                                            <div class="geodir-category-opt clearfix">
                                                                                                <div class="listing-rating-count-wrap flex-items-center">
                                                                                                    <div class="review-score">4.3</div>
                                                                                                    <div class="review-details">
                                                                                                        <div class="listing-rating card-popup-rainingvis" data-rating="4.3"></div>
                                                                                                        <div class="reviews-count">1 comment</div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="lcfields-wrap dis-flex"></div>
                                                                                        </div>
                                                                                        <div class="azp_element preview_listing_content azp-element-azp-0mypklfokr6 geodir-category-content">
                                                                                            <div class="geodir-category-content-title fl-wrap">
                                                                                                <div class="geodir-category-content-title-item">
                                                                                                    <h3 class="title-sin_map">
                                                                                                        <a href="https://townhub.cththemes.com/listing/monteplaza-hotel/">Asya Beauty</a>
                                                                                                    </h3>
                                                                                                    <div class="geodir-category-location fl-wrap">
                                                                                                        <a href="https://www.google.com/maps/search/?api=1&query=29.95106579999999,-90.0715323" class="map-item" target="_blank">
                                                                                                            <i class="fas fa-map-marker-alt"></i>KUCHING, KUCHING</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-text fl-wrap"> Sed interdum metus at nisi tempor laoreet. Integer ... <div class="lcfields-wrap dis-flex"></div>
                                                                                                <div class="facilities-list fl-wrap dis-flex flw-wrap">
                                                                                                    <div class="facilities-list-title">Facilities:</div>
                                                                                                    <ul class="no-list-style mrg-0 dis-inflex">
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Breakfast">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/breakfast/">
                                                                                                                <i class="fal fa-concierge-bell"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Free Parking">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/free-parking/">
                                                                                                                <i class="fal fa-parking"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Free Wi Fi">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/free-wi-fi/">
                                                                                                                <i class="fal fa-wifi"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Restaurant Inside">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/restaurant-inside/">
                                                                                                                <i class="fal fa-utensils"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="lcard-price">Price: <strong>$&nbsp;122.00</strong>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-footer fl-wrap dis-flex">
                                                                                                <div class="listing-cats-wrap dis-flex">
                                                                                                    <a href="https://townhub.cththemes.com/listing_cat/02-hotels/" class="listing-item-category-wrap flex-items-center">
                                                                                                        <div class="listing-item-category yellow-bg">
                                                                                                            <i class="far fa-bed"></i>
                                                                                                        </div>
                                                                                                        <span>Hotels</span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="price-level geodir-category_price dis-flex">
                                                                                                    <span class="price-level-item" data-pricerating="2"></span>
                                                                                                    <span class="price-name-tooltip">Moderate</span>
                                                                                                </div>
                                                                                                <div class="geodir-opt-list dis-flex">
                                                                                                    <ul class="no-list-style">
                                                                                                        <li class="lcard-bot-infos">
                                                                                                            <a href="#" class="show_gcc">
                                                                                                                <i class="fal fa-envelope"></i>
                                                                                                                <span class="geodir-opt-tooltip">Contact Info</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="geodir-category_contacts">
                                                                                                    <div class="close_gcc">
                                                                                                        <i class="fal fa-times-circle"></i>
                                                                                                    </div>
                                                                                                    <ul class="no-list-style">
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-phone"></i> Call : </span>
                                                                                                            <a href="tel:+016-5420384">+016-5420384</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-envelope"></i> Write : </span>
                                                                                                            <a href="/cdn-cgi/l/email-protection#176e7862657a767e7b5773787a767e793974787a">
                                                                                                                <span class="__cf_email__" data-cfemail="067f6973746b676f6a4662696b676f682865696b">[email&#160;protected]</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-link"></i>
                                                                                                                    </span>
                                                                                                            <a href="https://cththemes.com">Visit website</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </article>
                                                                                </div>
                                                                                <!-- listing-item end-->
                                                                                <!-- listing-item -->
                                                                                <div class="cthiso-item listing-item-loop post-5126 listing type-listing status-publish has-post-thumbnail hentry listing_cat-03-events listing_feature-air-conditioned listing_feature-airport-shuttle listing_feature-breakfast listing_feature-elevator-in-building listing_feature-free-parking listing_feature-free-wi-fi listing_feature-mini-bar listing_feature-restaurant-inside listing_feature-tv-inside listing_location-01-new-york listing_tag-hostel listing_tag-hotel listing_tag-parking listing_tag-restourant listing_tag-room listing_tag-spa" data-postid="5126">
                                                                                    <article class="geodir-category-listing fl-wrap">
                                                                                        <div class="azp_element preview_listing azp-element-azp-2kxm7x7j47m geodir-category-img">
                                                                                            <div class="geodir-js-favorite_btn">
                                                                                                <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                    <i class="fal fa-heart"></i>
                                                                                                    <span>Save</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="listing-featured">Featured</div>
                                                                                            <a href="https://townhub.cththemes.com/listing/a-consortium-of-music/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                <img width="382" height="252" style="height:252px;" class="respimg lazy" alt="" loading="lazy" data-src="https://dummyimage.com/270x270/db9f46/ffffff.jpg&text=BT" data-lazy="https://dummyimage.com/270x270/db9f46/ffffff.jpg&text=BT" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                                                <div class="overlay"></div>
                                                                                            </a>
                                                                                            <div class="listing-avatar">
                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://dummyimage.com/270x270/db9f46/ffffff.jpg&text=BT" data-lazy="https://dummyimage.com/270x270/db9f46/ffffff.jpg&text=BT">
                                                                                                </a>
                                                                                                <span class="avatar-tooltip lpre-avatar">Added By <strong>CTHthemes</strong>
                                                                                                        </span>
                                                                                            </div>
                                                                                            <div class="geodir_status_date lstatus-opening">
                                                                                                <i class="fal fa-clock"></i>July 3, 2022 8:30 am
                                                                                            </div>
                                                                                            <div class="geodir-category-opt clearfix">
                                                                                                <div class="listing-rating-count-wrap flex-items-center">
                                                                                                    <div class="review-score">4.0</div>
                                                                                                    <div class="review-details">
                                                                                                        <div class="listing-rating card-popup-rainingvis" data-rating="4.0"></div>
                                                                                                        <div class="reviews-count">1 comment</div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="lcfields-wrap dis-flex"></div>
                                                                                        </div>
                                                                                        <div class="azp_element preview_listing_content azp-element-azp-ggr3yufgsme geodir-category-content">
                                                                                            <div class="geodir-category-content-title fl-wrap">
                                                                                                <div class="geodir-category-content-title-item">
                                                                                                    <h3 class="title-sin_map">
                                                                                                        <a href="https://townhub.cththemes.com/listing/a-consortium-of-music/">Bidayuh traditional collection</a>
                                                                                                        <span class="verified-badge">
                                                                                                                    <i class="fal fa-check"></i>
                                                                                                                </span>
                                                                                                    </h3>
                                                                                                    <div class="geodir-category-location fl-wrap">
                                                                                                        <a href="https://www.google.com/maps/search/?api=1&query=40.78708237136644,-73.97548763779922" class="map-item" target="_blank">
                                                                                                            <i class="fas fa-map-marker-alt"></i>-</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-text fl-wrap"> Sed interdum metus at nisi tempor laoreet. Integer ... <div class="lcfields-wrap dis-flex"></div>
                                                                                                <div class="facilities-list fl-wrap dis-flex flw-wrap">
                                                                                                    <div class="facilities-list-title">Facilities:</div>
                                                                                                    <ul class="no-list-style mrg-0 dis-inflex">
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Air Conditioned">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/air-conditioned/">
                                                                                                                <i class="fal fa-snowflake"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Airport Shuttle">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/airport-shuttle/">
                                                                                                                <i class="fal fa-plane"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Breakfast">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/breakfast/">
                                                                                                                <i class="fal fa-concierge-bell"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Elevator in building">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/elevator-in-building/">
                                                                                                                <i class="fal fa-rocket"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="lcard-price">Price: <strong>$&nbsp;49.00</strong>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-footer fl-wrap dis-flex">
                                                                                                <div class="listing-cats-wrap dis-flex">
                                                                                                    <a href="https://townhub.cththemes.com/listing_cat/03-events/" class="listing-item-category-wrap flex-items-center">
                                                                                                        <div class="listing-item-category purp-bg">
                                                                                                            <i class="fas fa-cocktail"></i>
                                                                                                        </div>
                                                                                                        <span>Events</span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="geodir-opt-list dis-flex">
                                                                                                    <ul class="no-list-style">
                                                                                                        <li class="lcard-bot-infos">
                                                                                                            <a href="#" class="show_gcc">
                                                                                                                <i class="fal fa-envelope"></i>
                                                                                                                <span class="geodir-opt-tooltip">Contact Info</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="lcard-bot-gallery">
                                                                                                            <div class="dynamic-gal gdop-list-link" data-dynamicPath='[{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/50.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/51.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/49.jpg"}]'>
                                                                                                                <i class="fal fa-search-plus"></i>
                                                                                                                <span class="geodir-opt-tooltip">Gallery</span>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="geodir-category_contacts">
                                                                                                    <div class="close_gcc">
                                                                                                        <i class="fal fa-times-circle"></i>
                                                                                                    </div>
                                                                                                    <ul class="no-list-style">
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-phone"></i> Call : </span>
                                                                                                            <a href="tel:+60198343778">+60198343778</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-envelope"></i> Write : </span>
                                                                                                            <a href="/cdn-cgi/l/email-protection#4f36203a3d222e26230f2b20222e2621612c2022">
                                                                                                                <span class="__cf_email__" data-cfemail="047d6b717669656d6844606b69656d6a2a676b69">[email&#160;protected]</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-link"></i>
                                                                                                                    </span>
                                                                                                            <a href="https://cththemes.com">Visit website</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </article>
                                                                                </div>
                                                                                <!-- listing-item end-->
                                                                                <!-- listing-item -->
                                                                                <div class="cthiso-item listing-item-loop post-1888 listing type-listing status-publish has-post-thumbnail hentry listing_cat-03-events listing_feature-air-conditioned listing_feature-airport-shuttle listing_feature-breakfast listing_feature-free-parking listing_feature-free-wi-fi listing_feature-mini-bar listing_feature-restaurant-inside listing_location-01-new-york" data-postid="1888">
                                                                                    <article class="geodir-category-listing fl-wrap">
                                                                                        <div class="azp_element preview_listing azp-element-azp-2kxm7x7j47m geodir-category-img">
                                                                                            <div class="geodir-js-favorite_btn">
                                                                                                <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                    <i class="fal fa-heart"></i>
                                                                                                    <span>Save</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <a href="https://townhub.cththemes.com/listing/rocko-band-in-marquee-club/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                <img width="382" height="252" style="height:252px;" class="respimg lazy" alt="" loading="lazy" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14473/5c5cf719be508-1549596441.jpg" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14473/5c5cf719be508-1549596441.jpg" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                                                <div class="overlay"></div>
                                                                                            </a>
                                                                                            <div class="listing-avatar">
                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14473/5c5cf719be508-1549596441.jpg" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14473/5c5cf719be508-1549596441.jpg">
                                                                                                </a>
                                                                                                <span class="avatar-tooltip lpre-avatar">Added By <strong>CTHthemes</strong>
                                                                                                        </span>
                                                                                            </div>
                                                                                            <div class="geodir_status_date lstatus-opening">
                                                                                                <i class="fal fa-clock"></i>June 15, 2022 8:30 am
                                                                                            </div>
                                                                                            <div class="geodir-category-opt clearfix">
                                                                                                <div class="listing-rating-count-wrap flex-items-center">
                                                                                                    <div class="review-score">4.3</div>
                                                                                                    <div class="review-details">
                                                                                                        <div class="listing-rating card-popup-rainingvis" data-rating="4.3"></div>
                                                                                                        <div class="reviews-count">1 comment</div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="lcfields-wrap dis-flex"></div>
                                                                                        </div>
                                                                                        <div class="azp_element preview_listing_content azp-element-azp-ggr3yufgsme geodir-category-content">
                                                                                            <div class="geodir-category-content-title fl-wrap">
                                                                                                <div class="geodir-category-content-title-item">
                                                                                                    <h3 class="title-sin_map">
                                                                                                        <a href="https://townhub.cththemes.com/listing/rocko-band-in-marquee-club/">De'Aura Beauty Spa</a>
                                                                                                        <span class="verified-badge">
                                                                                                                    <i class="fal fa-check"></i>
                                                                                                                </span>
                                                                                                    </h3>
                                                                                                    <div class="geodir-category-location fl-wrap">
                                                                                                        <a href="https://www.google.com/maps/search/?api=1&query=40.8227727,-74.01575029999998" class="map-item" target="_blank">
                                                                                                            <i class="fas fa-map-marker-alt"></i>Kuching, Sarawak</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-text fl-wrap"> Sed interdum metus at nisi tempor laoreet. Integer ... <div class="lcfields-wrap dis-flex"></div>
                                                                                                <div class="facilities-list fl-wrap dis-flex flw-wrap">
                                                                                                    <div class="facilities-list-title">Facilities:</div>
                                                                                                    <ul class="no-list-style mrg-0 dis-inflex">
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Air Conditioned">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/air-conditioned/">
                                                                                                                <i class="fal fa-snowflake"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Airport Shuttle">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/airport-shuttle/">
                                                                                                                <i class="fal fa-plane"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Breakfast">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/breakfast/">
                                                                                                                <i class="fal fa-concierge-bell"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Free Parking">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/free-parking/">
                                                                                                                <i class="fal fa-parking"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="lcard-price">Price: <strong>$&nbsp;49.00</strong>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-footer fl-wrap dis-flex">
                                                                                                <div class="listing-cats-wrap dis-flex">
                                                                                                    <a href="https://townhub.cththemes.com/listing_cat/03-events/" class="listing-item-category-wrap flex-items-center">
                                                                                                        <div class="listing-item-category purp-bg">
                                                                                                            <i class="fas fa-cocktail"></i>
                                                                                                        </div>
                                                                                                        <span>Events</span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="price-level geodir-category_price dis-flex">
                                                                                                    <span class="price-level-item" data-pricerating="2"></span>
                                                                                                    <span class="price-name-tooltip">Moderate</span>
                                                                                                </div>
                                                                                                <div class="geodir-opt-list dis-flex">
                                                                                                    <ul class="no-list-style">
                                                                                                        <li class="lcard-bot-infos">
                                                                                                            <a href="#" class="show_gcc">
                                                                                                                <i class="fal fa-envelope"></i>
                                                                                                                <span class="geodir-opt-tooltip">Contact Info</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="lcard-bot-gallery">
                                                                                                            <div class="dynamic-gal gdop-list-link" data-dynamicPath='[{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/50.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/51.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/49.jpg"}]'>
                                                                                                                <i class="fal fa-search-plus"></i>
                                                                                                                <span class="geodir-opt-tooltip">Gallery</span>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="geodir-category_contacts">
                                                                                                    <div class="close_gcc">
                                                                                                        <i class="fal fa-times-circle"></i>
                                                                                                    </div>
                                                                                                    <ul class="no-list-style">
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-phone"></i> Call : </span>
                                                                                                            <a href="tel:+60178284985">+60178284985</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-envelope"></i> Write : </span>
                                                                                                            <a href="/cdn-cgi/l/email-protection#5b22342e29363a32371b3f34363a323575383436">
                                                                                                                <span class="__cf_email__" data-cfemail="8ef7e1fbfce3efe7e2ceeae1e3efe7e0a0ede1e3">[email&#160;protected]</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-link"></i>
                                                                                                                    </span>
                                                                                                            <a href="https://cththemes.com">Visit website</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </article>
                                                                                </div>
                                                                                <!-- listing-item end-->
                                                                                <!-- listing-item -->
                                                                                <div class="cthiso-item listing-item-loop post-5502 listing type-listing status-publish has-post-thumbnail hentry listing_cat-04-fitness listing_feature-air-conditioned listing_feature-airport-shuttle listing_feature-breakfast listing_feature-elevator-in-building listing_feature-free-parking listing_feature-free-wi-fi listing_feature-pet-friendly listing_feature-restaurant-inside listing_location-it listing_location-04-rome listing_tag-cocktails listing_tag-food listing_tag-friendly-service listing_tag-lunch listing_tag-sandwich listing_tag-wine" data-postid="5502">
                                                                                    <article class="geodir-category-listing fl-wrap">
                                                                                        <div class="azp_element preview_listing azp-element-azp-sgfq927d1s geodir-category-img">
                                                                                            <div class="geodir-js-favorite_btn">
                                                                                                <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                    <i class="fal fa-heart"></i>
                                                                                                    <span>Save</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="listing-featured">Featured</div>
                                                                                            <a href="https://townhub.cththemes.com/listing/gym-in-the-center/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                <img width="382" height="252" style="height:252px;" class="respimg lazy" alt="" loading="lazy" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14464/5c74b75bcd150-1551152987.jpg" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14464/5c74b75bcd150-1551152987.jpg" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                                                <div class="overlay"></div>
                                                                                            </a>
                                                                                            <div class="listing-avatar">
                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14464/5c74b75bcd150-1551152987.jpg" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14464/5c74b75bcd150-1551152987.jpg">
                                                                                                </a>
                                                                                                <span class="avatar-tooltip lpre-avatar">Added By <strong>CTHthemes</strong>
                                                                                                        </span>
                                                                                            </div>
                                                                                            <div class="lcard-saleoff">
                                                                                                <div class="saleoff-inner">Sale 35%</div>
                                                                                            </div>
                                                                                            <div class="geodir_status_date lstatus-closed">
                                                                                                <i class="fal fa-lock"></i>Now Closed
                                                                                            </div>
                                                                                            <div class="lcfields-wrap dis-flex"></div>
                                                                                        </div>
                                                                                        <div class="azp_element preview_listing_content azp-element-azp-e49ww3xidf9 geodir-category-content">
                                                                                            <div class="geodir-category-content-title fl-wrap">
                                                                                                <div class="geodir-category-content-title-item">
                                                                                                    <h3 class="title-sin_map">
                                                                                                        <a href="https://townhub.cththemes.com/listing/gym-in-the-center/">Erra Akil Food Processing Enterprise</a>
                                                                                                        <span class="verified-badge">
                                                                                                                    <i class="fal fa-check"></i>
                                                                                                                </span>
                                                                                                    </h3>
                                                                                                    <div class="geodir-category-location fl-wrap">
                                                                                                        <a href="https://www.google.com/maps/search/?api=1&query=41.90297514567541,12.498060656097437" class="map-item" target="_blank">
                                                                                                            <i class="fas fa-map-marker-alt"></i>Asajaya,Kuching, Sarawak</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-text fl-wrap"> Sed interdum metus at nisi tempor laoreet. Integer ... <div class="lcfields-wrap dis-flex"></div>
                                                                                                <div class="facilities-list fl-wrap dis-flex flw-wrap">
                                                                                                    <div class="facilities-list-title">Facilities:</div>
                                                                                                    <ul class="no-list-style mrg-0 dis-inflex">
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Air Conditioned">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/air-conditioned/">
                                                                                                                <i class="fal fa-snowflake"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Airport Shuttle">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/airport-shuttle/">
                                                                                                                <i class="fal fa-plane"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Breakfast">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/breakfast/">
                                                                                                                <i class="fal fa-concierge-bell"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Elevator in building">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/elevator-in-building/">
                                                                                                                <i class="fal fa-rocket"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="lcard-price">Price: <strong>$&nbsp;15.00</strong>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-footer fl-wrap dis-flex">
                                                                                                <div class="listing-cats-wrap dis-flex">
                                                                                                    <a href="https://townhub.cththemes.com/listing_cat/04-fitness/" class="listing-item-category-wrap flex-items-center">
                                                                                                        <div class="listing-item-category blue-bg">
                                                                                                            <i class="far fa-dumbbell"></i>
                                                                                                        </div>
                                                                                                        <span>Fitness</span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="price-level geodir-category_price dis-flex">
                                                                                                    <span class="price-level-item" data-pricerating="4"></span>
                                                                                                    <span class="price-name-tooltip">Ultra High</span>
                                                                                                </div>
                                                                                                <div class="geodir-opt-list dis-flex">
                                                                                                    <ul class="no-list-style">
                                                                                                        <li class="lcard-bot-infos">
                                                                                                            <a href="#" class="show_gcc">
                                                                                                                <i class="fal fa-envelope"></i>
                                                                                                                <span class="geodir-opt-tooltip">Contact Info</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="lcard-bot-gallery">
                                                                                                            <div class="dynamic-gal gdop-list-link" data-dynamicPath='[{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/09\/10.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/25-1.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/26-2.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/52.jpg"}]'>
                                                                                                                <i class="fal fa-search-plus"></i>
                                                                                                                <span class="geodir-opt-tooltip">Gallery</span>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="geodir-category_contacts">
                                                                                                    <div class="close_gcc">
                                                                                                        <i class="fal fa-times-circle"></i>
                                                                                                    </div>
                                                                                                    <ul class="no-list-style">
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-phone"></i> Call : </span>
                                                                                                            <a href="tel:+01119185798">+01119185798</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-envelope"></i> Write : </span>
                                                                                                            <a href="/cdn-cgi/l/email-protection#fa83958f88979b9396ba9e95979b9394d4999597">
                                                                                                                <span class="__cf_email__" data-cfemail="c0b9afb5b2ada1a9ac80a4afada1a9aeeea3afad">[email&#160;protected]</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-link"></i>
                                                                                                                    </span>
                                                                                                            <a href="https://cththemes.com">Visit website</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </article>
                                                                                </div>
                                                                                <!-- listing-item end-->
                                                                                <!-- listing-item -->
                                                                                <div class="cthiso-item listing-item-loop post-5497 listing type-listing status-publish has-post-thumbnail hentry listing_cat-04-fitness listing_feature-air-conditioned listing_feature-airport-shuttle listing_feature-breakfast listing_feature-elevator-in-building listing_feature-free-parking listing_feature-free-wi-fi listing_feature-mini-bar listing_feature-restaurant-inside listing_feature-tv-inside listing_location-en listing_location-05-london listing_tag-hostel listing_tag-hotel listing_tag-parking listing_tag-restourant listing_tag-room listing_tag-spa" data-postid="5497">
                                                                                    <article class="geodir-category-listing fl-wrap">
                                                                                        <div class="azp_element preview_listing azp-element-azp-sgfq927d1s geodir-category-img">
                                                                                            <div class="geodir-js-favorite_btn">
                                                                                                <a href="#" class="save-btn logreg-modal-open" data-message="Logging in first to save this listing.">
                                                                                                    <i class="fal fa-heart"></i>
                                                                                                    <span>Save</span>
                                                                                                </a>
                                                                                            </div>
                                                                                            <div class="listing-featured">Featured</div>
                                                                                            <a href="https://townhub.cththemes.com/listing/premium-fitness-gym/" class="listing-thumb-link geodir-category-img-wrap fl-wrap">
                                                                                                <img width="382" height="252" style="height:252px;" class="respimg lazy" alt="" loading="lazy" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14469/5c7755fc4f7e5-1551324668.jpg" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14469/5c7755fc4f7e5-1551324668.jpg" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                                                <div class="overlay"></div>
                                                                                            </a>
                                                                                            <div class="listing-avatar">
                                                                                                <a href="https://townhub.cththemes.com/author/admin/">
                                                                                                    <img loading="lazy" alt="CTHthemes" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" class="avatar avatar-80 photo lazy" height="80" width="80" data-src="https://ukuyacommunity.s3.amazonaws.com/default/organization/14469/5c7755fc4f7e5-1551324668.jpg" data-lazy="https://ukuyacommunity.s3.amazonaws.com/default/organization/14469/5c7755fc4f7e5-1551324668.jpg">
                                                                                                </a>
                                                                                                <span class="avatar-tooltip lpre-avatar">Added By <strong>CTHthemes</strong>
                                                                                                        </span>
                                                                                            </div>
                                                                                            <div class="geodir_status_date lstatus-closed">
                                                                                                <i class="fal fa-lock"></i>Now Closed
                                                                                            </div>
                                                                                            <div class="lcfields-wrap dis-flex"></div>
                                                                                        </div>
                                                                                        <div class="azp_element preview_listing_content azp-element-azp-e49ww3xidf9 geodir-category-content">
                                                                                            <div class="geodir-category-content-title fl-wrap">
                                                                                                <div class="geodir-category-content-title-item">
                                                                                                    <h3 class="title-sin_map">
                                                                                                        <a href="https://townhub.cththemes.com/listing/premium-fitness-gym/">Hariza Spa & Beauty</a>
                                                                                                        <span class="verified-badge">
                                                                                                                    <i class="fal fa-check"></i>
                                                                                                                </span>
                                                                                                    </h3>
                                                                                                    <div class="geodir-category-location fl-wrap">
                                                                                                        <a href="https://www.google.com/maps/search/?api=1&query=40.7869996,-73.97553089999997" class="map-item" target="_blank">
                                                                                                            <i class="fas fa-map-marker-alt"></i>Kuching, Sarawak</a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-text fl-wrap"> Sed interdum metus at nisi tempor laoreet. Integer ... <div class="lcfields-wrap dis-flex"></div>
                                                                                                <div class="facilities-list fl-wrap dis-flex flw-wrap">
                                                                                                    <div class="facilities-list-title">Facilities:</div>
                                                                                                    <ul class="no-list-style mrg-0 dis-inflex">
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Air Conditioned">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/air-conditioned/">
                                                                                                                <i class="fal fa-snowflake"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Airport Shuttle">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/airport-shuttle/">
                                                                                                                <i class="fal fa-plane"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Breakfast">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/breakfast/">
                                                                                                                <i class="fal fa-concierge-bell"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="tolt" data-microtip-position="top" data-tooltip="Elevator in building">
                                                                                                            <a href="https://townhub.cththemes.com/listing_feature/elevator-in-building/">
                                                                                                                <i class="fal fa-rocket"></i>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="lcard-price">Price: <strong>$&nbsp;15.00</strong>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="geodir-category-footer fl-wrap dis-flex">
                                                                                                <div class="listing-cats-wrap dis-flex">
                                                                                                    <a href="https://townhub.cththemes.com/listing_cat/04-fitness/" class="listing-item-category-wrap flex-items-center">
                                                                                                        <div class="listing-item-category blue-bg">
                                                                                                            <i class="far fa-dumbbell"></i>
                                                                                                        </div>
                                                                                                        <span>Fitness</span>
                                                                                                    </a>
                                                                                                </div>
                                                                                                <div class="geodir-opt-list dis-flex">
                                                                                                    <ul class="no-list-style">
                                                                                                        <li class="lcard-bot-infos">
                                                                                                            <a href="#" class="show_gcc">
                                                                                                                <i class="fal fa-envelope"></i>
                                                                                                                <span class="geodir-opt-tooltip">Contact Info</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li class="lcard-bot-gallery">
                                                                                                            <div class="dynamic-gal gdop-list-link" data-dynamicPath='[{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/09\/10.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/25-1.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/26-2.jpg"},{"src":"https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/08\/52.jpg"}]'>
                                                                                                                <i class="fal fa-search-plus"></i>
                                                                                                                <span class="geodir-opt-tooltip">Gallery</span>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                                <div class="geodir-category_contacts">
                                                                                                    <div class="close_gcc">
                                                                                                        <i class="fal fa-times-circle"></i>
                                                                                                    </div>
                                                                                                    <ul class="no-list-style">
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-phone"></i> Call : </span>
                                                                                                            <a href="tel:+0167685458">+0167685458</a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-envelope"></i> Write : </span>
                                                                                                            <a href="/cdn-cgi/l/email-protection#bec7d1cbccd3dfd7d2fedad1d3dfd7d090ddd1d3">
                                                                                                                <span class="__cf_email__" data-cfemail="c8b1a7bdbaa5a9a1a488aca7a5a9a1a6e6aba7a5">[email&#160;protected]</span>
                                                                                                            </a>
                                                                                                        </li>
                                                                                                        <li>
                                                                                                                    <span>
                                                                                                                        <i class="fal fa-link"></i>
                                                                                                                    </span>
                                                                                                            <a href="https://cththemes.com">Visit website</a>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </article>
                                                                                </div>
                                                                                <!-- listing-item end-->
                                                                            </div>
                                                                            <div class="view-all-listings">
                                                                                <a href="https://townhub.cththemes.com/listing/" class="btn  dec_btn  color2-bg">Check Out All Listings <i class="fal fa-arrow-alt-right"></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                        <!-- end col-md-12 -->
                                                                    </div>
                                                                    <!-- end row -->
                                                                </div>
                                                                <!-- end container -->
                                                            </div>
                                                            <!-- list-main-wrap end-->
                                                        </div>
                                                        <!--  listings-grid-wrap end-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="elementor-section elementor-top-section elementor-element elementor-element-3dacf8a gray-bg elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="3dacf8a" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3c4ef10" data-id="3c4ef10" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-a5be474 elementor-widget elementor-widget-our_partners" data-id="a5be474" data-element_type="widget" data-widget_type="our_partners.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="clients-carousel-wrap fl-wrap">
                                                            <div class="cc-btn cc-prev">
                                                                <i class="fal fa-angle-left"></i>
                                                            </div>
                                                            <div class="cc-btn cc-next">
                                                                <i class="fal fa-angle-right"></i>
                                                            </div>
                                                            <div class="clients-carousel">
                                                                <div class="swiper-container">
                                                                    <div class="swiper-wrapper">
                                                                        <!--client-item-->
                                                                        <div class="swiper-slide">
                                                                            <a class="client-item" href="https://jquery.com/" target="_blank">
                                                                                <img width="240" height="50" class="attachment-partner size-partner lazy" alt="" loading="lazy" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/1.png" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/1.png" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                            </a>
                                                                        </div>
                                                                        <!--client-item end-->
                                                                        <!--client-item-->
                                                                        <div class="swiper-slide">
                                                                            <a class="client-item" href="https://envato.com/" target="_blank">
                                                                                <img width="240" height="50" class="attachment-partner size-partner lazy" alt="" loading="lazy" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/2.png" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/2.png" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                            </a>
                                                                        </div>
                                                                        <!--client-item end-->
                                                                        <!--client-item-->
                                                                        <div class="swiper-slide">
                                                                            <a class="client-item" href="https://wordpress.org/" target="_blank">
                                                                                <img width="240" height="50" class="attachment-partner size-partner lazy" alt="" loading="lazy" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/3.png" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/3.png" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                            </a>
                                                                        </div>
                                                                        <!--client-item end-->
                                                                        <!--client-item-->
                                                                        <div class="swiper-slide">
                                                                            <a class="client-item" href="https://jquery.com/" target="_blank">
                                                                                <img width="240" height="50" class="attachment-partner size-partner lazy" alt="" loading="lazy" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/1-1.png" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/1-1.png" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                            </a>
                                                                        </div>
                                                                        <!--client-item end-->
                                                                        <!--client-item-->
                                                                        <div class="swiper-slide">
                                                                            <a class="client-item" href="https://envato.com/" target="_blank">
                                                                                <img width="240" height="50" class="attachment-partner size-partner lazy" alt="" loading="lazy" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/2-1.png" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/2-1.png" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                            </a>
                                                                        </div>
                                                                        <!--client-item end-->
                                                                        <!--client-item-->
                                                                        <div class="swiper-slide">
                                                                            <a class="client-item" href="https://wordpress.org/" target="_blank">
                                                                                <img width="240" height="50" class="attachment-partner size-partner lazy" alt="" loading="lazy" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/3-1.png" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/3-1.png" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                                                            </a>
                                                                        </div>
                                                                        <!--client-item end-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
        <!-- Content end -->
    </div>
    <!-- wrapper end -->
    <!--cart  -->
    <div class="show-cart color2-bg">
        <i class="far fa-shopping-cart"></i>
        <span class="cart-count">0</span>
    </div>
    <div class="cart-overlay"></div>
    <div class="cart-modal">
        <div class="cart-modal-wrap fl-wrap">
                    <span class="close-cart color2-bg">Close <i class="fal fa-times"></i>
                    </span>
            <h3>Your cart</h3>
            <div class="widget_shopping_cart_content"></div>
        </div>
    </div>
    <!--cart end-->
    <!--footer -->
    <footer class="townhub-footer main-footer dark-footer  ">
        <div class="footer-header fl-wrap grad ient-dark">
            <div class="container footer_widgets_top">
                <div class="row fhwids-row dis-flex flw-wrap">
                    <div class="dynamic-footer-widget col-md-5 col-sm-5 col-xs-12">
                        <div id="custom_html-11" class="widget_text footer-widget widget widget_custom_html">
                            <div class="textwidget custom-html-widget">
                                <div class="subscribe-header">
                                    <h3>Subscribe For a <span>Newsletter</span>
                                    </h3>
                                    <p>Whant to be notified about new locations ? Just sign up.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dynamic-footer-widget col-md-7 col-sm-7 col-xs-12">
                        <div id="custom_html-10" class="widget_text footer-widget widget widget_custom_html">
                            <div class="textwidget custom-html-widget">
                                <div class="subscribe-form ">
                                    <form class="townhub_mailchimp-form">
                                        <div class="subscribe-form-wrap">
                                            <input class="enteremail" id="subscribe-email" name="email" placeholder="Enter Your Email" type="email" required="required">
                                            <button type="submit" class="subscribe-button">
                                                <i class="fal fa-envelope"></i>
                                            </button>
                                        </div>
                                        <label class="subscribe-agree-label" for="subscribe-agree-checkbox">
                                            <input id="subscribe-agree-checkbox" type="checkbox" name="sub-agree-terms" required="required" value="1">I agree with the <a href="https://townhub.cththemes.com/privacy-policy/">Privacy Policy</a>
                                        </label>
                                        <label for="subscribe-email" class="subscribe-message"></label>
                                        <input type="hidden" name="_nonce" value="cc0e2b377c">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-inner fl-wrap">
            <div class="container footer_widgets">
                <div class="row fwids-row">
                    <div class="dynamic-footer-widget col-sm-12 col-md-4">
                        <div id="media_image-2" class="footer-widget fl-wrap widget_media_image">
                            <a href="#" class="footer-logo-link">
                                <img width="411" height="77" class="image wp-image-6459  attachment-full size-full lazy" alt="" loading="lazy" style="max-width: 100%; height: auto;" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/logo.png" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/logo.png" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                            </a>
                        </div>
                        <div id="custom_html-6" class="widget_text footer-widget fl-wrap widget_custom_html">
                            <div class="textwidget custom-html-widget">
                                <div class="footer-contacts-widget fl-wrap">
                                    <p>In ut odio libero, at vulputate urna. Nulla tristique mi a massa convallis cursus. Nulla eu mi magna. Etiam suscipit commodo gravida. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.</p>
                                    <ul class="footer-contacts fl-wrap">
                                        <li>
                                                    <span>
                                                        <i class="fal fa-envelope"></i> Mail : </span>
                                            <a href="#" target="_blank" rel="noopener">
                                                <span class="__cf_email__" data-cfemail="bfc6d0cacdd2ded6d3ffdbd0d2ded6d191dcd0d2">[email&#160;protected]</span>
                                            </a>
                                        </li>
                                        <li>
                                                    <span>
                                                        <i class="fal fa-map-marker-alt"></i> Adress : </span>
                                            <a href="#" target="_blank" rel="noopener">USA 27TH Brooklyn NY</a>
                                        </li>
                                        <li>
                                                    <span>
                                                        <i class="fal fa-phone"></i> Phone : </span>
                                            <a href="#">+7(111)123456789</a>
                                        </li>
                                    </ul>
                                    <div class="footer-social">
                                        <span>Find us : </span>
                                        <ul>
                                            <li>
                                                <a href="#" target="_blank" rel="noopener">
                                                    <i class="fab fa-facebook-f"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" target="_blank" rel="noopener">
                                                    <i class="fab fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" target="_blank" rel="noopener">
                                                    <i class="fab fa-instagram"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" target="_blank" rel="noopener">
                                                    <i class="fab fa-vk"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dynamic-footer-widget col-sm-12 col-md-4">
                        <div id="townhub-recent-posts-2" class="footer-widget fl-wrap townhub_recent_posts">
                            <h3 class="wid-tit">Our Last News</h3>
                            <div class="widget-posts fl-wrap">
                                <ul class="no-list-style">
                                    <li class="widget-posts-item clearfix">
                                        <a href="https://townhub.cththemes.com/heres-what-people-are-saying-about-us/" class="widget-posts-img">
                                            <img width="98" height="65" class="respimg wp-post-image lazy" alt="" loading="lazy" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/7-1-98x65.jpg" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/7-1-98x65.jpg" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                        </a>
                                        <div class="widget-posts-descr">
                                            <a href="https://townhub.cththemes.com/heres-what-people-are-saying-about-us/" title="Here’s What People Are Saying About Us">Here’s What People Are Saying About Us</a>
                                            <span class="widget-posts-date">
                                                        <span class="post-date">
                                                            <i class="fal fa-calendar"></i> September 7, 2019 </span>
                                                    </span>
                                        </div>
                                    </li>
                                    <li class="widget-posts-item clearfix">
                                        <a href="https://townhub.cththemes.com/in-hac-habitasse-platea/" class="widget-posts-img">
                                            <img width="98" height="65" class="respimg wp-post-image lazy" alt="" loading="lazy" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/29-1-98x65.jpg" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/29-1-98x65.jpg" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                        </a>
                                        <div class="widget-posts-descr">
                                            <a href="https://townhub.cththemes.com/in-hac-habitasse-platea/" title="In hac habitasse platea">In hac habitasse platea</a>
                                            <span class="widget-posts-date">
                                                        <span class="post-date">
                                                            <i class="fal fa-calendar"></i> June 7, 2019 </span>
                                                    </span>
                                        </div>
                                    </li>
                                    <li class="widget-posts-item clearfix">
                                        <a href="https://townhub.cththemes.com/integer-sagittis/" class="widget-posts-img">
                                            <img width="98" height="65" class="respimg wp-post-image lazy" alt="" loading="lazy" data-src="https://townhub.cththemes.com/wp-content/uploads/2019/08/43-98x65.jpg" data-lazy="https://townhub.cththemes.com/wp-content/uploads/2019/08/43-98x65.jpg" src="https://townhub.cththemes.com/wp-content/uploads/2019/12/pixel-5x1.png" />
                                        </a>
                                        <div class="widget-posts-descr">
                                            <a href="https://townhub.cththemes.com/integer-sagittis/" title="Integer sagittis">Integer sagittis</a>
                                            <span class="widget-posts-date">
                                                        <span class="post-date">
                                                            <i class="fal fa-calendar"></i> May 25, 2019 </span>
                                                    </span>
                                        </div>
                                    </li>
                                </ul>
                                <a href="https://townhub.cththemes.com/news/" class="footer-link">Read all <i class="fal fa-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="dynamic-footer-widget col-sm-12 col-md-4">
                        <div id="townhub-twitter-feed-1" class="footer-widget fl-wrap townhub_twitter_feed">
                            <h3 class="wid-tit">Our Twitter</h3>
                            <div class="twitter-holder fl-wrap scrollbar-inner2" data-simplebar data-simplebar-auto-hide="false">
                                <div class="tweet townhub-tweet tweet-count-4 tweet-ticker-no" data-username="CTHthemes" data-list="" data-hashtag="" data-ticker="no" data-count="4"></div>
                            </div>
                            <div class="follow-wrap">
                                <a href="#" target="_blank" class="twiit-button footer-link twitter-link"> Follow Us <i class="fal fa-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer bg-->
            <div class="footer-bg" data-ran="4"></div>
            <div class="footer-wave">
                <svg viewbox="0 0 100 25">
                    <path fill="#fff" d="M0 30 V12 Q30 17 55 12 T100 11 V30z" />
                </svg>
            </div>
            <!-- footer bg  end-->
        </div>
        <div class="sub-footer fl-wrap">
            <div class="container">
                <div class="row flex-items-center sub-footer-row flw-wrap">
                    <div class="col-md-6 col-sm-12 col-xs-12 subfooter-info-wrap">
                        <div class="copyright">
                            <span class="ft-copy">&#169; <a href="https://themeforest.net/user/cththemes" target="_blank">CTHthemes</a> 2020. All rights reserved. </span>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 subfooter-menu-wrap">
                        <div id="nav_menu-2" class="townhub-footer-menu widget_nav_menu">
                            <h3 class="widget-title widget-title-hide">Footer Menu</h3>
                            <div class="menu-footer-menu-container">
                                <ul id="menu-footer-menu" class="menu">
                                    <li id="menu-item-6723" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6723">
                                        <a href="#">Terms of use</a>
                                    </li>
                                    <li id="menu-item-6724" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-6724">
                                        <a href="#">Privacy Policy</a>
                                    </li>
                                    <li id="menu-item-6725" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6725">
                                        <a href="https://townhub.cththemes.com/news/">Blog</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="custom_html-9" class="widget_text townhub-footer-menu widget_custom_html">
                            <h3 class="widget-title widget-title-hide">Languages</h3>
                            <div class="textwidget custom-html-widget">
                                <!--<div class="lang-wrap"><div class="show-lang"><span><i class="fal fa-globe-europe"></i><strong>En</strong></span><i class="fa fa-caret-down arrlan"></i></div><ul class="lang-tooltip lang-action no-list-style"><li><a href="#" class="current-lan" data-lantext="En">English</a></li><li><a href="#" data-lantext="Fr">Français</a></li><li><a href="#" data-lantext="Es">Español</a></li><li><a href="#" data-lantext="De">Deutsch</a></li></ul></div>-->
                                <div class="currency-wrap">
                                    <div class="show-currency-tooltip">
                                        <i class="currency-symbol">$</i>
                                        <span>USD <i class="fa fa-caret-down"></i>
                                                </span>
                                    </div>
                                    <ul class="currency-tooltip currency-switcher">
                                        <li>
                                            <a class="currency-item" href="/?currency=EUR">
                                                <i class="currency-symbol">€</i>EUR </a>
                                        </li>
                                        <li>
                                            <a class="currency-item" href="/?currency=VND">
                                                <i class="currency-symbol">₫</i>VND </a>
                                        </li>
                                        <li>
                                            <a class="currency-item" href="/?currency=ZAR">
                                                <i class="currency-symbol">R</i>ZAR </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--footer end  -->
    <a class="to-top">
        <i class="fas fa-caret-up"></i>
    </a>
</div>
<!-- Main end -->
<div id="chat-app"></div>
<script type="text/template" id="tmpl-load-listings"> <div class="listings-loader">
        <div class="lload-icon-wrap">
            <i class="fal fa-spinner fa-pulse fa-3x"></i>
        </div>
        <div class="lload-text-wrap">Loading</div>
    </div>
</script>
<script type="text/template" id="tmpl-no-results"> <div class="no-results-search">
        <h2>No Results</h2>
        <p>There are no listings matching your search.</p>
        <p>Try changing your search filters or
            <a href="https://townhub.cththemes.com" class="reset-filter-link">Reset Filter</a>
        </p>
    </div>
</script>
<script type="text/template" id="tmpl-map-info"><#
    var one_review_text = "{REVIEW} review",
    other_review_text = "{REVIEW} reviews";
    #>
    <div class="map-popup-wrap">
        <div class="map-popup">
            <div class="infoBox-close">
                <i class="fal fa-times"></i>
            </div>
            <a href="" class="listing-img-content fl-wrap">
                <div class="infobox-status wkhour- "></div>
                <img src="" alt=""><# if( data.rating.rating ){ #>
                <div class="card-popup-raining map-card-rainting" data-rating="5" data-stars="5">
																																											<span class="map-popup-reviews-count">( <# print( data.rating.count > 1 ? other_review_text.replace('{REVIEW}', data.rating.count) : one_review_text.replace('{REVIEW}', data.rating.count)  ) #> )
																																											</span>
                </div><# } #>
            </a>
            <div class="listing-content">
                <div class="listing-content-item fl-wrap">


                    <div class="listing-title fl-wrap">
                        <h4>
                            <a href=""></a>
                        </h4>
                        <div class="map-popup-location-info">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                    </div>
                    <div class="map-popup-footer">
                        <a href="" class="main-link">Details
                            <i class="fal fa-long-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script type="text/template" id="tmpl-feature-search"><# _.each(data.features, function(fea){ #>
    <div class="listing-feature-wrap">
        <input id="features_" type="checkbox" value="" name="lfeas[]">
        <label for="features_"></label>
    </div>
    <!-- end listing-feature-wrap --><# }) #>
</script>
<script type="text/template" id="tmpl-filter-subcats"><# _.each(data.subcats, function(subcat){ #>
    <div class="listing-feature-wrap">
        <input id="filter_subcats_" type="checkbox" value="" name="filter_subcats[]">
        <label for="filter_subcats_"></label>
    </div>
    <!-- end listing-feature-wrap --><# }) #>
</script>
<div id="ol-popup" class="ol-popup">
    <a href="#" id="ol-popup-closer" class="ol-popup-closer"></a>
    <div id="ol-popup-content"></div>
</div>
<script type="text/javascript">
    (function() {
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    })();
</script>
<!--register form -->
<div class="main-register-wrap ctb-modal-wrap ctb-modal" id="ctb-logreg-modal">
    <div class="reg-overlay"></div>
    <div class="main-register-holder ctb-modal-holder tabs-act">
        <div class="main-register ctb-modal-inner fl-wrap  modal_main tabs-wrapper">
            <div class="ctb-modal-title">Welcome to <span>
                            <strong>Town</strong>Hub <strong>.</strong>
                        </span>
            </div>
            <div class="close-reg ctb-modal-close">
                <i class="fal fa-times"></i>
            </div>
            <div class="prelog-message"></div>
            <ul class="tabs-menu fl-wrap no-list-style">
                <li class="current">
                    <a href="#tab-login">
                        <i class="fal fa-sign-in-alt"></i> Login </a>
                </li>
                <li>
                    <a href="#tab-register">
                        <i class="fal fa-user-plus"></i> Register </a>
                </li>
            </ul>
            <!--tabs -->
            <div class="tabs-container">
                <div class="tab">
                    <!--tab -->
                    <div id="tab-login" class="tab-content first-tab">
                        <div class="custom-form">
                            <form method="post" id="townhub-login">
                                <div class="log-message"></div>
                                <label for="user_login">Username or Email Address <span>*</span>
                                </label>
                                <input id="user_login" name="log" type="text" onClick="this.select()" value="" required>
                                <label for="user_pass">Password <span>*</span>
                                </label>
                                <input id="user_pass" name="pwd" type="password" onClick="this.select()" value="" required>
                                <div id="loginCaptcha" class="cth-recaptcha"></div>
                                <button type="submit" id="log-submit" class="log-submit-btn btn color2-bg">Log In <i class="fas fa-caret-right"></i>
                                </button>
                                <div class="clearfix"></div>
                                <div class="filter-tags">
                                    <input name="rememberme" id="rememberme" value="true" type="checkbox">
                                    <label for="rememberme">Remember me</label>
                                </div>
                                <input type="hidden" id="_loginnonce" name="_loginnonce" value="9e42b72f5c" />
                                <input type="hidden" name="_wp_http_referer" value="/" />
                                <input type="hidden" name="redirection" value="https://townhub.cththemes.com" />
                            </form>
                            <div class="lost_password">
                                <a class="lost-password" href="https://townhub.cththemes.com/forget-password-page/">Lost Your Password?</a>
                            </div>
                        </div>
                    </div>
                    <!--tab end -->
                    <!--tab -->
                    <div class="tab">
                        <div id="tab-register" class="tab-content">
                            <div class="custom-form">
                                <form method="post" class="main-register-form" id="townhub-register">
                                    <div class="reg-message"></div>
                                    <p>Account details will be confirmed via email.</p>
                                    <label for="reg_username">Username <span>*</span>
                                    </label>
                                    <input id="reg_username" name="username" type="text" onClick="this.select()" value="" required pattern="^[A-Za-z\d\.]{4,}$" title="You can use letters, numbers and periods and at least 6 characters or more">
                                    <span class="input-pattern-desc">You can use letters, numbers and periods and at least 6 characters or more</span>
                                    <label for="reg_email">Email Address <span>*</span>
                                    </label>
                                    <input id="reg_email" name="email" type="email" onClick="this.select()" value="" required pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Make sure to enter all lowercase letters for your email address">
                                    <span class="input-pattern-desc">Make sure to enter all lowercase letters for your email address</span>
                                    <div class="terms_wrap">
                                        <div class="filter-tags">
                                            <input id="accept_term" name="accept_term" value="1" type="checkbox" required="required">
                                            <label for="accept_term">By using the website, you accept the terms and conditions</label>
                                        </div>
                                        <div class="filter-tags">
                                            <input id="consent_data" name="consent_data" value="1" type="checkbox" required="required">
                                            <label for="consent_data">Consent to processing of personal data</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="regCaptcha" class="cth-recaptcha"></div>
                                    <button type="submit" id="reg-submit" class="log-submit-btn btn color2-bg">Register <i class="fas fa-caret-right"></i>
                                    </button>
                                    <input type="hidden" id="_regnonce" name="_regnonce" value="bd1715a3b2" />
                                    <input type="hidden" name="_wp_http_referer" value="/" />
                                    <input type="hidden" name="redirection" value="https://townhub.cththemes.com" />
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--tab end -->
                </div>
                <!--tabs end -->
                <div class="log-separator fl-wrap">
                    <span>or</span>
                </div>
                <div class="soc-log fl-wrap">
                    <script>
                        jQuery(".mo_btn-mo").prop("disabled", false);
                    </script>
                    <script type="text/javascript">
                        jQuery(document).ready(function() {
                            jQuery(".login-button").css("cursor", "pointer");
                        });

                        function mo_openid_on_consent_change(checkbox) {
                            if (!checkbox.checked) {
                                jQuery('#mo_openid_consent_checkbox').val(1);
                                jQuery(".mo_btn-mo").attr("disabled", true);
                                jQuery(".login-button").addClass("dis");
                            } else {
                                jQuery('#mo_openid_consent_checkbox').val(0);
                                jQuery(".mo_btn-mo").attr("disabled", false);
                                jQuery(".login-button").removeClass("dis");
                            }
                        }
                        var perfEntries = performance.getEntriesByType("navigation");
                        if (perfEntries[0].type === "back_forward") {
                            location.reload(true);
                        }

                        function HandlePopupResult(result) {
                            window.location = "https://townhub.cththemes.com";
                        }

                        function moOpenIdLogin(app_name, is_custom_app) {
                            var current_url = window.location.href;
                            var cookie_name = "redirect_current_url";
                            var d = new Date();
                            d.setTime(d.getTime() + (2 * 24 * 60 * 60 * 1000));
                            var expires = "expires=" + d.toUTCString();
                            document.cookie = cookie_name + "=" + current_url + ";" + expires + ";path=/";
                            var base_url = 'https://townhub.cththemes.com';
                            var request_uri = '/';
                            var http = 'https://';
                            var http_host = 'townhub.cththemes.com';
                            var default_nonce = '8a92432c99';
                            var custom_nonce = 'e35d26e775';
                            if (is_custom_app == 'false') {
                                if (request_uri.indexOf('wp-login.php') != -1) {
                                    var redirect_url = base_url + '/?option=getmosociallogin&wp_nonce=' + default_nonce + '&app_name=';
                                } else {
                                    var redirect_url = http + http_host + request_uri;
                                    if (redirect_url.indexOf('?') != -1) {
                                        redirect_url = redirect_url + '&option=getmosociallogin&wp_nonce=' + default_nonce + '&app_name=';
                                    } else {
                                        redirect_url = redirect_url + '?option=getmosociallogin&wp_nonce=' + default_nonce + '&app_name=';
                                    }
                                }
                            } else {
                                if (request_uri.indexOf('wp-login.php') != -1) {
                                    var redirect_url = base_url + '/?option=oauthredirect&wp_nonce=' + custom_nonce + '&app_name=';
                                } else {
                                    var redirect_url = http + http_host + request_uri;
                                    if (redirect_url.indexOf('?') != -1) redirect_url = redirect_url + '&option=oauthredirect&wp_nonce=' + custom_nonce + '&app_name=';
                                    else redirect_url = redirect_url + '?option=oauthredirect&wp_nonce=' + custom_nonce + '&app_name=';
                                }
                            }
                            if (0) {
                                var myWindow = window.open(redirect_url + app_name, "", "width=700,height=620");
                            } else {
                                window.location.href = redirect_url + app_name;
                            }
                        }
                    </script>
                    <p>For faster login or register use your social account.</p>
                    <div class='mo-openid-app-icons'>
                        <p style='color:#7D93B2; width: fit-content;'></p>
                        <a rel='nofollow' style='margin-left: 4px !important;width: 200px !important;padding-top:6px !important;padding-bottom:6px !important;margin-bottom: -1px !important;border-radius: 4px !important;' class='mo_btn mo_btn-mo mo_btn-block mo_btn-social mo_btn-facebook mo_btn-custom-dec login-button mo_btn_transform_i  ' onClick="moOpenIdLogin('facebook','true');">
                            <i class='fab fa-facebook' style='padding-top:0px !important; margin-top: 0' src='https://townhub.cththemes.com/wp-content/plugins/miniorange-login-openid/includes/images/icons/facebook.png'></i>Login with Facebook </a>
                    </div>
                    <br>
                    <div style='float:left;margin-bottom: 0px;margin-top: 0px;' class='mo_image_id'>
                        <a target='_blank' href='https://www.miniorange.com/'>
                            <img alt='logo' src='https://townhub.cththemes.com/wp-content/plugins/miniorange-login-openid/includes/images/miniOrange.png' class='mo_openid_image'>
                        </a>
                    </div>
                    <br />
                    <br />
                </div>
                <div class="wave-bg">
                    <div class='wave -one'></div>
                    <div class='wave -two'></div>
                </div>
            </div>
        </div>
        <!-- main-register end -->
    </div>
</div>
<!--register form end -->
<div class="ctb-modal-wrap ctb-modal" id="ctb-resetpsw-modal">
    <div class="ctb-modal-holder">
        <div class="ctb-modal-inner modal_main">
            <div class="ctb-modal-close">
                <i class="fal fa-times"></i>
            </div>
            <div class="ctb-modal-title">Reset <span>
                            <strong>Password</strong>
                        </span>
            </div>
            <div class="ctb-modal-content">
                <form class="reset-password-form custom-form" action="#" method="post">
                    <fieldset>
                        <label for="user_reset">Username or Email Address <span>*</span>
                        </label>
                        <input id="user_reset" name="user_login" type="text" value="" required>
                    </fieldset>
                    <button type="submit" class="btn color2-bg">Get New Password <i class="fas fa-caret-right"></i>
                    </button>
                </form>
            </div>
            <!-- end modal-content -->
        </div>
    </div>
</div>
<!-- end reset password modal -->
<script type='text/javascript' src='https://townhub.cththemes.com/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.9' id='regenerator-runtime-js'></script>
<script type='text/javascript' src='https://townhub.cththemes.com/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0' id='wp-polyfill-js'></script>
<script type='text/javascript' id='contact-form-7-js-extra'>
    /*
																																																			<![CDATA[ */
    var wpcf7 = {
        "api": {
            "root": "https:\/\/townhub.cththemes.com\/wp-json\/",
            "namespace": "contact-form-7\/v1"
        }
    };
    /* ]]> */
</script>
<script type='text/javascript' id='townhub-addons-js-extra'>
    /*
																																																			<![CDATA[ */
    var _townhub_add_ons = {
        "url": "/admin-ajax.php",
        "nonce": "09f4c5ee05",
        "posted_on": "Posted on ",
        "reply": "Reply",
        "retweet": "Retweet",
        "favorite": "Favorite",
        "pl_w": "Please wait...",
        "like": "Like",
        "unlike": "Unlike",
        "use_dfmarker": "",
        "hide_mkprice": "",
        "marker": "https:\/\/townhub.cththemes.com\/wp-content\/uploads\/2019\/10\/shop.png",
        "center_lat": "40.7",
        "center_lng": "-73.87",
        "map_zoom": "18",
        "socials": {
            "facebook-f": "Facebook",
            "twitter": "Twitter",
            "youtube": "Youtube",
            "vimeo-v": "Vimeo",
            "instagram": "Instagram",
            "vk": "Vkontakte",
            "reddit": "Reddit",
            "pinterest-p": "Pinterest",
            "vine": "Vine Camera",
            "tumblr": "Tumblr",
            "flickr": "Flickr",
            "google-plus-g": "Google+",
            "linkedin-in": "LinkedIn",
            "whatsapp": "Whatsapp",
            "meetup": "Meetup",
            "odnoklassniki": "Odnoklassniki",
            "envelope": "Email",
            "telegram": "Telegram",
            "custom_icon": "Custom"
        },
        "gmap_type": "ROADMAP",
        "login_delay": "3000",
        "listing_type_opts": [],
        "chatbox_message": "We are here to help. Please ask us anything or share your feedback",
        "post_id": "545",
        "ckot_url": "https:\/\/townhub.cththemes.com\/listing-checkout\/",
        "location_type": "administrative_area_level_2",
        "autocomplete_result_type": "none",
        "address_format": [],
        "country_restrictions": [""],
        "place_lng": "en",
        "disable_bubble": "yes",
        "lb_approved": "Approved",
        "lb_24h": "1",
        "td_color": "#4DB7FE",
        "lb_delay": "3000",
        "md_limit": "3",
        "md_limit_msg": "Max upload files is 3",
        "md_limit_size": "2",
        "md_limit_size_msg": "Max upload file size is 2 MB",
        "search": "Search...",
        "gcaptcha": "1",
        "gcaptcha_key": "6LdcxvsSAAAAAEt8-RuMzQb3X1nAqtt_3g_2Mg0V",
        "location_show_state": "yes",
        "weather_unit": "metric",
        "weather_strings": {
            "days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "min": "Min",
            "max": "Max",
            "direction": ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"]
        },
        "i18n": {
            "share_on": "Share this on {SOCIAL}",
            "del-listing": "Are you sure want to delete  listing and its data?\nThe listing is permanently deleted.",
            "cancel-booking": "Are you sure want to cancel  booking?",
            "approve-booking": "Are you sure want to approve  booking?",
            "del-booking": "Are you sure want to delete  booking and its data?\nThe booking is permanently deleted.",
            "del-message": "Are you sure want to cancel  message?",
            "chats_h3": "Inbox",
            "chat_fr_owner": "Chat With Owner",
            "chat_fr_login": "Login to chat",
            "chat_fr_cwith": "Chat with ",
            "chat_fr_conver": "Conversations",
            "change_pas_h3": " Change Password",
            "change_pas_lb_CP": "Current Password",
            "change_pas_lb_NP": "New Password",
            "change_pas_lb_CNP": "Confirm New Password",
            "inner_chat_op_W": "Week",
            "inner_chat_op_M": "Month",
            "inner_chat_op_Y": "Year",
            "inner_listing_li_E": "Edit ",
            "inner_listing_li_D": "Delete ",
            "author_review_h3": "Reviews for your listings",
            "likebtn": "Like Button",
            "welcome": "Welcome",
            "listings": "Listings",
            "bookings": "Bookings",
            "reviews": "Reviews",
            "log_out": "Log Out ",
            "add_hour": "Add Hour",
            "book_dates": "Dates",
            "book_services": "Extra Services",
            "book_ad": "ADULTS",
            "book_chi": "CHILDREN",
            "book_avr": "Available Rooms",
            "book_ts": "Total Cost",
            "book_chev": "Check availability",
            "book_bn": "Book Now",
            "checkout_can": "Cancel",
            "checkout_app": "Apply",
            "roomsl_avai": "Available:",
            "roomsl_maxg": "Max Guests: ",
            "roomsl_quan": "Quantity",
            "btn_save": "Save Change",
            "btn_save_c": "Save Changes",
            "btn_close": "Close me",
            "btn_send": "Send",
            "btn_add_F": "Add Fact",
            "fact_title": "Fact Title",
            "fact_number": "Fact Number",
            "fact_icon": "Fact Icon",
            "location_country": "Country",
            "location_state": "State",
            "location_city": "City",
            "faq_title": "Question",
            "faq_content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "btn_add_Faq": "Add FAQ",
            "btn_add_S": "Add Social",
            "btn_add_R": "Add Room",
            "btn_add_N": "Add New",
            "image_upload": " Click here to upload",
            "th_mount": "Amount",
            "th_method": "Method",
            "th_to": "To",
            "th_date": "Date Submitted",
            "th_status": "Status",
            "calendar_dis_number": "Select the number of months displayed.",
            "calendar_number_one": "One Months",
            "calendar_number_two": "Two Months",
            "calendar_number_three": "Three Months",
            "calendar_number_four": "Four Months",
            "calendar_number_five": "Five Months",
            "calendar_number_six": "Six Months",
            "calendar_number_seven": "Seven Months",
            "coupon_code": "Coupon code",
            "coupon_discount": "Discount type",
            "coupon_percentage": "Percentage discount",
            "coupon_fix_cart": "Fixed cart discount",
            "coupon_desc": "Description",
            "coupon_show": "Display content in widget banner?",
            "coupon_amount": "Discount amount",
            "coupon_qtt": "Coupon quantity",
            "coupon_expiry": "Coupon expiry date",
            "coupon_format": "Format:YY-mm-dd HH:ii:ss",
            "bt_coupon": "Add Coupon",
            "bt_services": "Add Service",
            "services_name": "Service Name",
            "services_desc": "Description",
            "services_price": "Service Price",
            "bt_member": "Add Member",
            "member_name": "Name: ",
            "member_job": "Job or Position: ",
            "member_desc": "Description",
            "member_img": "Image",
            "memeber_social": "Socials",
            "member_url": "Website",
            "days": ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
            "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            "earnings_title": "Your Earnings",
            "th_date_": "Date",
            "th_total_": "Total",
            "th_fee_": "Author Fee",
            "th_earning_": "Earning",
            "th_order_": "Order",
            "go_back": "Go back",
            "no_earning": "You have no earning.",
            "th_vat_ser": "VAT - Services",
            "cancel": "Cancel",
            "submit": "Submit",
            "ltype_title": "Listing type",
            "ltype_desc": "Listing type description",
            "wkh_enter": "Enter Hours",
            "wkh_open": "Open all day",
            "wkh_close": "Close all day",
            "calen_lock": "Lock this month",
            "calen_unlock": "Unlock this month",
            "smwdtitle": "Submit a withdrawal request",
            "wdfunds": "Withdraw funds",
            "goearnings": "View Earnings",
            "chat_type_msg": "Type Message",
            "save": "Save",
            "cal_event_start": "Event start time: ",
            "cal_event_end": "Event end date: ",
            "cal_opts": "Options",
            "wth_payments": "PayPal \/ Stripe Email",
            "wth_amount": "Amount ",
            "wth_plh_email": "email@gmail.com",
            "wth_acount_balance": "Account Balance",
            "wth_will_process": "Your request will be processed on {DATE}",
            "wth_no_request": "You have no withdrawal request",
            "wth_min_str": "The minimum withdrawal amount is {amount}",
            "wth_max_str": "The maximum withdrawal amount is {amount}",
            "wth_bank_iban": "IBAN",
            "wth_bank_account": "ACOUNT",
            "wth_bank_name": "NAME",
            "wth_bank_bname": "Bank Name",
            "wth_enter_email": "Please enter a correct email address.",
            "bt_slots": "Add Time Slot",
            "slot_time": "Time",
            "slot_guests": "Guests",
            "slot_available": "Available slots",
            "no_ltype": "There is no listing type. Please contact to site owner for more details.",
            "ltype_select_guide": "Click to change listing type",
            "bt_add_menu": "Add Menu",
            "menu_name": "Menu Name",
            "menu_cats": "Menu Types (comma separated)",
            "menu_desc": "Menu Description",
            "menu_price": "Menu Price",
            "menu_url": "Menu Link",
            "menu_photos": "Menu Photos",
            "headm_iframe": "iFrame Source",
            "headm_mp4": "MP4 Video",
            "headm_youtube": "Youtube Video ID",
            "headm_vimeo": "Vimeo Video ID",
            "headm_bgimg": "Background Image",
            "preview_btn": "Preview",
            "add_listing": "Add Listing",
            "edit_listing": "Edit Listing",
            "add_room": "Add Room",
            "edit_room": "Edit Room",
            "nights": "Nights",
            "slots_add": "Add Slot",
            "slots_guests": "Max Guests",
            "slots_start": "Start time",
            "slots_end": "End time",
            "slots_price": "Price",
            "raselect_placeholder": "Select",
            "raselect_nooptions": "No options",
            "cal_bulkedit": "Bulk Edit",
            "save_bulkedit": "Save",
            "cancel_bulkedit": "Cancel",
            "adults": "Adults",
            "children": "Children",
            "AM": "AM",
            "PM": "PM",
            "evt_start": "Start",
            "evt_end": "End",
            "no_slots": "There is no available slot. Select another date.",
            "slots_avai": "{slots} slots available",
            "no_tickets": "There is no available tickets. Select another date.",
            "tickets_required": "You need select a ticket",
            "no_rooms": "There is no room available in the selected period. Please select another period then click on Check availability button",
            "no_rooms_init": "Click on Check availability button to see available rooms",
            "cal_clear_past": "Clear old dates",
            "field_required": "{SMFNAME} field is required. Please enter it value."
        },
        "distance_df": "10000",
        "curr_user": {
            "id": 0,
            "display_name": "",
            "avatar": "",
            "can_upload": false,
            "role": false,
            "is_author": false
        },
        "currency": {
            "currency": "USD",
            "symbol": "$",
            "rate": "1.00",
            "sb_pos": "left_space",
            "decimal": 2,
            "ths_sep": ",",
            "dec_sep": "."
        },
        "base_currency": {
            "currency": "USD",
            "symbol": "$",
            "rate": "1.00",
            "sb_pos": "left_space",
            "decimal": 2,
            "ths_sep": ",",
            "dec_sep": "."
        },
        "wpml": null,
        "unfill_address": "",
        "unfill_state": "",
        "unfill_city": "",
        "js_decimals": "2",
        "map_provider": "mapbox",
        "mbtoken": "pk.eyJ1IjoiY3RodGhlbWVzIiwiYSI6ImNrY2ZscmNwYzBodTgzM284NHhwczR4NTYifQ.tiV5QneMfPPyYIIXSDUktw",
        "week_starts_monday": "yes",
        "withdrawal_date": "15",
        "single_map_init": ""
    };
    var _townhub_dashboard = {
        "i18n": {
            "inner_chat_op_W": "Week",
            "inner_chat_op_M": "Month",
            "inner_chat_op_Y": "Year",
            "chart_alltime": "All time",
            "chart_views_lbl": "Listing Views",
            "chart_earnings_lbl": "Earnings",
            "chart_bookings_lbl": "Bookings",
            "withdrawals": "Withdrawals",
            "wth_notes": "Additional Infos"
        },
        "chart_hide_views": "",
        "chart_hide_earning": "",
        "chart_hide_booking": "1",
        "payment": {
            "submitform": {
                "title": "Submit Form",
                "icon": "",
                "desc": "<p>Your payment details will be submitted for review.<\/p>",
                "checkout_text": "Place Order"
            },
            "cod": {
                "title": "Cash on delivery",
                "icon": "",
                "desc": "<p>Your payment details will be submitted. Then pay on delivery.<\/p>",
                "checkout_text": "Place Order"
            },
            "banktransfer": {
                "title": "Bank Transfer",
                "icon": "https:\/\/townhub.cththemes.com\/wp-content\/plugins\/townhub-add-ons\/assets\/images\/bank-transfer.png",
                "desc": "<p>\r\n<strong>Bank name<\/strong>: Bank of America, NA<br \/>\r\n<strong>Bank account number<\/strong>: 0175380000<br \/>\r\n<strong>Bank address<\/strong>:USA 27TH Brooklyn NY<br \/>\r\n<strong>Bank SWIFT code<\/strong>: BOFAUS 3N<br \/>\r\n<\/p>",
                "checkout_text": "Place Order"
            },
            "paypal": {
                "title": "Paypal",
                "icon": "https:\/\/townhub.cththemes.com\/wp-content\/plugins\/townhub-add-ons\/assets\/images\/ppcom.png",
                "desc": "<p>Pay via PayPal; you can pay with your credit card if you don\u2019t have a PayPal account.<\/p>",
                "checkout_text": "Process to Paypal"
            },
            "stripe": {
                "title": "Stripe",
                "icon": "https:\/\/townhub.cththemes.com\/wp-content\/plugins\/townhub-add-ons\/assets\/images\/stripe.png",
                "desc": "<p>Pay via Stripe; you can pay with your credit card.<\/p>",
                "checkout_text": "Pay Now"
            },
            "payfast": {
                "title": "Pay via Payfast",
                "icon": "https:\/\/townhub.cththemes.com\/wp-content\/plugins\/townhub-add-ons\/assets\/images\/payfast.png",
                "desc": "<p>Pay via Payfast; you can pay with your credit card.<\/p>\r\n<p>NOTE: This payment method does not included in theme package<\/p>",
                "checkout_text": "Process to Payfast"
            },
            "skrill": {
                "title": "Skrill",
                "icon": "https:\/\/www.skrill.com\/fileadmin\/content\/images\/brand_centre\/Skrill_Logos\/skrill-85x37_en.gif",
                "desc": "<p>Pay via Skrill; you can pay with your credit card.<\/p>\r\n<p>NOTE: This payment method does not included in theme package<\/p>",
                "checkout_text": "Process to Skrill"
            },
            "paystack": {
                "title": "Paystack",
                "icon": "https:\/\/townhub.cththemes.com\/wp-content\/plugins\/townhub-add-ons\/assets\/images\/paystack.png",
                "desc": "<p>Pay via Payfast; you can pay with your credit card.<\/p>\r\n<p>NOTE: This payment method does not included in theme package<\/p>",
                "checkout_text": "Process to Paystack"
            },
            "2c2p": {
                "title": "2C2P",
                "icon": "https:\/\/townhub.cththemes.com\/wp-content\/plugins\/townhub-2c2p\/2c2p-logo.png",
                "desc": "<p>Pay via 2C2P; you can pay with your credit card.<\/p>\r\n<p>NOTE: This payment method does not included in theme package<\/p>",
                "checkout_text": "Process to 2C2P"
            },
            "razorpay": {
                "title": "Razorpay",
                "icon": "https:\/\/townhub.cththemes.com\/wp-content\/plugins\/townhub-razorpay\/assets\/razorpay-logo.png",
                "desc": "<p>Pay via Razorpay; you can pay with your credit card.<\/p>\r\n<p>NOTE: This payment method does not included in theme package<\/p>",
                "checkout_text": "Process to Razorpay"
            }
        },
        "withdrawal_min": "10"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='https://api.mapbox.com/mapbox-gl-js/v1.11.0/mapbox-gl.js' id='mapbox-gl-js'></script>
<script type='text/javascript' src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js' id='mapbox-gl-geocoder-js'></script>
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js' id='es6-promise-js'></script>
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js' id='es6-promise-auto-js'></script>
<script type='text/javascript' src='https://townhub.cththemes.com/wp-includes/js/dist/vendor/react.min.js?ver=17.0.1' id='react-js'></script>
<script type='text/javascript' src='https://townhub.cththemes.com/wp-includes/js/dist/vendor/react-dom.min.js?ver=17.0.1' id='react-dom-js'></script>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?onload=cthCaptchaCallback&#038;render=explicit' async='async' defer='defer' id='g-recaptcha-js'></script>
<script type='text/javascript' id='wc-add-to-cart-js-extra'>
    /*
																																																			<![CDATA[ */
    var wc_add_to_cart_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
        "i18n_view_cart": "View cart",
        "cart_url": "https:\/\/townhub.cththemes.com\/woo-cart\/",
        "is_cart": "",
        "cart_redirect_after_add": "no"
    };
    /* ]]> */
</script>
<script type='text/javascript' id='woocommerce-js-extra'>
    /*
																																																			<![CDATA[ */
    var woocommerce_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
    };
    /* ]]> */
</script>
<script type='text/javascript' id='wc-cart-fragments-js-extra'>
    /*
																																																			<![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
        "cart_hash_key": "wc_cart_hash_a60acc80d15f8d44b285604f64257cbb",
        "fragment_name": "wc_fragments_a60acc80d15f8d44b285604f64257cbb",
        "request_timeout": "5000"
    };
    /* ]]> */
</script>
<script type='text/javascript' id='townhub-scripts-js-extra'>
    /*
																																																			<![CDATA[ */
    var _townhub = {
        "hheight": "80"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?render=6LdhhdcZAAAAAAhjyYENyez85IM7y3DXFNLLvi7J&#038;ver=3.0' id='google-recaptcha-js'></script>
<script type='text/javascript' id='wpcf7-recaptcha-js-extra'>
    /*
																																																			<![CDATA[ */
    var wpcf7_recaptcha = {
        "sitekey": "6LdhhdcZAAAAAAhjyYENyez85IM7y3DXFNLLvi7J",
        "actions": {
            "homepage": "homepage",
            "contactform": "contactform"
        }
    };
    /* ]]> */
</script>
<script type='text/javascript' id='elementor-frontend-js-before'>
    var elementorFrontendConfig = {
        "environmentMode": {
            "edit": false,
            "wpPreview": false,
            "isScriptDebug": false
        },
        "i18n": {
            "shareOnFacebook": "Share on Facebook",
            "shareOnTwitter": "Share on Twitter",
            "pinIt": "Pin it",
            "download": "Download",
            "downloadImage": "Download image",
            "fullscreen": "Fullscreen",
            "zoom": "Zoom",
            "share": "Share",
            "playVideo": "Play Video",
            "previous": "Previous",
            "next": "Next",
            "close": "Close"
        },
        "is_rtl": false,
        "breakpoints": {
            "xs": 0,
            "sm": 480,
            "md": 768,
            "lg": 1025,
            "xl": 1440,
            "xxl": 1600
        },
        "responsive": {
            "breakpoints": {
                "mobile": {
                    "label": "Mobile",
                    "value": 767,
                    "default_value": 767,
                    "direction": "max",
                    "is_enabled": true
                },
                "mobile_extra": {
                    "label": "Mobile Extra",
                    "value": 880,
                    "default_value": 880,
                    "direction": "max",
                    "is_enabled": false
                },
                "tablet": {
                    "label": "Tablet",
                    "value": 1024,
                    "default_value": 1024,
                    "direction": "max",
                    "is_enabled": true
                },
                "tablet_extra": {
                    "label": "Tablet Extra",
                    "value": 1200,
                    "default_value": 1200,
                    "direction": "max",
                    "is_enabled": false
                },
                "laptop": {
                    "label": "Laptop",
                    "value": 1366,
                    "default_value": 1366,
                    "direction": "max",
                    "is_enabled": false
                },
                "widescreen": {
                    "label": "Widescreen",
                    "value": 2400,
                    "default_value": 2400,
                    "direction": "min",
                    "is_enabled": false
                }
            }
        },
        "version": "3.6.5",
        "is_static": false,
        "experimentalFeatures": {
            "e_import_export": true,
            "e_hidden_wordpress_widgets": true,
            "landing-pages": true,
            "elements-color-picker": true,
            "favorite-widgets": true,
            "admin-top-bar": true
        },
        "urls": {
            "assets": "https:\/\/townhub.cththemes.com\/wp-content\/plugins\/elementor\/assets\/"
        },
        "settings": {
            "page": [],
            "editorPreferences": []
        },
        "kit": {
            "active_breakpoints": ["viewport_mobile", "viewport_tablet"],
            "global_image_lightbox": "yes",
            "lightbox_enable_counter": "yes",
            "lightbox_enable_fullscreen": "yes",
            "lightbox_enable_zoom": "yes",
            "lightbox_enable_share": "yes",
            "lightbox_title_src": "title",
            "lightbox_description_src": "description"
        },
        "post": {
            "id": 545,
            "title": "TownHub%20%E2%80%93%20Directory%20%26%20Listing%20WordPress%20Theme",
            "excerpt": "",
            "featuredImage": false
        }
    };
</script>
<script>
    function lazyListingsChanged() {
        // create new window event for listing change
        var evt = new CustomEvent('listingsChanged', {
            detail: 'lazy-load'
        });
        window.dispatchEvent(evt);
    }

    function lazyGalChanged(el) {
        // create new window event for listing change
        var evt = new CustomEvent('galChanged', {
            detail: el
        });
        window.dispatchEvent(evt);
    }
    // Set the options to make LazyLoad self-initialize
    window.lazyLoadOptions = {
        elements_selector: ".lazy",
        // ... more custom settings?
        callback_loaded: (el) => {
            // console.log("Loaded", el)
            if (window.listingItemsEle != null) {
                // console.log('has #listing-items');
                if (window.listingItemsEle.contains(el)) {
                    // console.log('el inside #listing-items');
                    lazyListingsChanged()
                }
            }
            lazyGalChanged(el)
        },
        callback_finish: () => {
            // console.log("Finish")
        },
    };
    // Listen to the initialization event and get the instance of LazyLoad
    window.addEventListener('LazyLoad::Initialized', function(event) {
        window.lazyLoadInstance = event.detail.instance;
        window.listingItemsEle = document.getElementById('listing-items')
    }, false);
</script>
<script defer src="https://townhub.cththemes.com/wp-content/cache/autoptimize/js/autoptimize_eba87b47e8faa62509641374c9735f7d.js"></script>
</body>
</html>
<!-- WP Fastest Cache file was created in 1.9013228416443 seconds, on 10-06-22 19:48:10 -->
